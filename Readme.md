# AnyScale Sensors

The goal of the project is to provide a testbed for Raspberry Pi nodes and potentially other devices to act as a data collection and compute platform for the Smart Campus project at the University of Glasgow.

## Milestones

1. Create two prototypes with PIR, Temperature, Luminosity sensors and WiFi and/or Bluetooth on the Raspberry Pi 1 with a centralized database server.
2. Scale to about 10 nodes and investigate further sensor options, such as measuring sound levels.
3. Create a custom PCB prototype and build a more integrated, lower cost, sensor platform.

## Components

### Sensors
- PIR motion detector, a single high/low alarm pin. Manual configuration of sensitivity by potentiometer.
- Temperature sensor, digital, I2C interface.
- Luminosity (light) sensor, digital, I2C interface.
- _TO Design/source: Microphone with envelope filter for analogue noise level meter. Needs ADC._

### Networking
- Wired network in the school of computing science
- WiFi using existing University services such as eduroam
- Mesh networking to be investigated 

### Power
- Assuming an available power outlet for the prototyping stages.

### Deployment and Management

How do we install, configure, and update the different pieces of software running on the distributed nodes?

### Sensor software

There will be a process dedicated to each sensor

### Server software

Which database software should we run, and how do we easily interface this with the consuming (apps, web interface) and producing (sensors) clients over the network? How do we provide security and authentication?

## Repository Layout and applications
A number of services / applications will be implemented to realise the proposed system. In general, documentation about a particular service may be found in the _Readme.md_ file inside the corresponding source directory.

- `deployment/` scripts and documentation for the deployment processes
- `src/` source code for applications and services
    - `protos/` protocol and service definitions
    - `pkg/` top level python package
        - `collector/` collector service
        - `common/` common modules
        - `heartbeat/` heartbeat service
        - `i2c/` i2c service
        - `measurements/` measurements service
        - `rest/` REST API server
        - `sensors/` sensing applications
            - `light/`
            - `temperature/`
            - `motion/`
        - `stream/` event streaming API server
