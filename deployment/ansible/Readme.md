# Ansible Deployment

## Files

* scripts
* files such as public keys fetched from remote hosts will end up in here

## Templates

* Application configuration
* Systemd service definitions

## Inventories

* Production
* Staging
* New Hosts

## Playbooks

### 1. Common System Setup

On first setup for the raspberry pi's we must specify the default user and type the sudo password. Later deployments should be able to use the deploy user without a password prompt.

```sh
# First run
ansible-playbook -s --ask-sudo-pass -u pi -k -i new_hosts common_setup.yml

# Re-runs (if needed) after ssh and user accounts are set up
ansible-playbook -s -u deploy -i production common_setup.yml
```

* Set hostname
* Create deploy user with key-based authentication and sudo rights
* Disable root login
* Disable password authentication
* Upgrade system packages and install common packages

### 2. Raspberry Pi First setup

```sh
ansible-playbook -s -u deploy -i production rpi_setup.yml
```

* Expand file system and reboot
* Enable I2C and SPI kernel modules

### 3. Generate Wheels

A single Raspberry Pi can compile the python dependencies for later distribution.

```sh
ansible-playbook -s -u deploy -i production rpi_build_wheels.yml
scp -r pi@pi6:/tmp/dependency-wheels/wheels/ files/rpi-wheels/
```

* copy requirements.txt
* Set up new virtual environment
* run pip/wheel
* copy wheels folder back to the admin host

TO DO: the fetch module does not support recursive fetching yet. The resulting files can be copied back with scp instead.

### 4. Setup server applications

```sh
ansible-playbook -s -u deploy -i production server.yml
```

* Copy python pkg folder from repository
* Set up folder and create virtualenv
* Install dependencies
* Install and configure mongodb
* Install, configure, and start heartbeat service
* Install, configure, and start measurement service
* Install, configure, and start restapi service
* Fetch server host key

### 5. Setup sensor applications

```sh
ansible-playbook -s -u deploy -i production sensors.yml
```

* Copy python pkg folder from repository
* Copy dependency wheels
* Set up folder and create virtualenv
* Install dependencies, preferably from wheels
* Install, configure, and start collector service
* Install, configure, and start i2c service if required
* Install, configure, and start sensor services if required
* Add the server's host key to known hosts

### 6. Copy SSH keys

The sensor application setup fetches public keys from each sensor node, these now need to be added to the server.

```sh
ansible-playbook -s -u deploy -i production server_add_keys.yml
```

* Add SSH keys to tunnel user's authorized_keys file.


### 7. Add Authorised keys to pi for admins 
To add keys to the authorized_keys list of the sensor pis, execute the ansible script from aukena. 

```sh
ansible-playbook -s -u deploy -i production sensor_add_keys.yml
```

* Add SSH keys to  authorized_keys file of sensor pi's. At the moment it can only run sucessfully from the deploy account of aukena 
