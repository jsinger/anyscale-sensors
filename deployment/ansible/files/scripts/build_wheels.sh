#!/bin/sh

DIR=$1

cd $DIR
virtualenv venv
source venv/bin/activate
pip install --upgrade pip wheel
pip wheel --wheel-dir wheels/ -r requirements.txt
