define(function(require) {
    var WebsocketModel  = require('model/websocket');
    var RestModel       = require('model/rest');
    var HostsView       = require('view/hosts');
    var GaugesView      = require('view/gauges');
    var TimelineView    = require('view/timeline');

    // Initialize the API endpoint connections
    // Default to aukena, unless the location hash is #localhost.
    var server_name = location.hash == '#localhost' ? 'localhost' : 'aukena.dcs.gla.ac.uk';

    var stream  = new WebsocketModel('ws://'+server_name+':5001');
    var rest    = new RestModel('http://'+server_name+':5000');

    // Hosts list works as a selection interface. It periodically requests the heartbeat status.
    var hosts = new HostsView(rest, document.getElementById('hosts'));

    // Line chart shows and requests current and historical data for all sensors on the current host
    var timeline = new TimelineView(rest, document.getElementById('timeline'));

    // Gauge chart shows only the most recent value for each sensor on the current host
    var gauges = new GaugesView(rest, document.getElementById('gauges'));

    // When a new host is selected, delete all data from the charts
    // and reset them to their initial state.
    var current_host = '';
    hosts.addListener(function(hostname, period) {
        console.log("selected: ", hostname, period);
        current_host = hostname;

        // The line chart will start fetching data for the newly selected host.
        timeline.reset(hostname, period);

        // The gauge chart will also fetch the current status for the new host by itself.
        gauges.reset(hostname);
    });

    // When the stream emits a message, the new value is added to the line chart
    // the gauges are also updated. Messages not matching the currently selected
    // host are ignored.
    stream.addListener(function(message) {
        if (message.node_name == current_host) {
            var date = new Date(message.timestamp*1000);
            timeline.update(message.sensor_type, date, message.value);
            gauges.update(message.sensor_type, date, message.value);

            console.log(message.channel, Math.round(message.value));
        }
    });

    // For debugging, log references to the views.
    console.log(hosts, timeline, gauges);
});
