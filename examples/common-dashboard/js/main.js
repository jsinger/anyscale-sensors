requirejs.config({
    baseUrl: 'js/lib',
    paths: {
        model: '../model',
        view: '../view',
        controller: '../controller',
        jquery: 'jquery-2.2.4.min'
    },
    urlArgs:'t=' + (new Date()).getTime()
});

requirejs(['controller/app']);
