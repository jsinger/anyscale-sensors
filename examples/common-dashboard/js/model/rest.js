/* Models the REST-style API for retrieving node information
 * and historical sensor readings.
 */

define(['jquery'], function() {
    var RestModel = function(base_url) {
        this.base_url = base_url;
    }

    RestModel.prototype.getNodes = function(cb) {
        this._dispatch("/node/", cb);
    }

    RestModel.prototype.getNode = function(node_name, cb) {
        this._dispatch("/node/" + node_name + "/", cb);
    }

    RestModel.prototype.getReadingsBefore = function(node_name, sensor_type, date, cb) {
        timestamp = parseInt(date.getTime() / 1000);
        this._dispatch("/node/" + node_name + "/" + sensor_type + "/?end=" + timestamp+"&limit=100", cb);
    }

    RestModel.prototype._dispatch = function(endpoint, cb) {
        /* Requests the given endpoint from the server as a GET request
         * and calls the callback with the response if it is okay,
         * otherwise calls the callback with a false value.
         */
        $.getJSON(this.base_url + endpoint, function(data, status){
            if(status != "success") {
                cb(false);
            } else {
                cb(data);
            }
        });
    }

    return RestModel;
});
