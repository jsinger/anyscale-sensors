/* Models a continuous stream of data readings coming in
 * and notifies registered callbacks on new readings.
 */

define(function() {
    var WebsocketModel = function(url) {
        this._listeners = [];
        this._url = url;

        var _this = this;

        var _socket_error = function(event) {
            console.warn("Websocket error:", event);
        }

        var _socket_close = function(event) {
            console.warn("Websocket closed:", event);
        }

        var _socket_message = function(event) {
            var data = JSON.parse(event.data);
            _notify_listeners(data);
        }

        var _notify_listeners = function(data) {
            for (var cb of _this._listeners) {
                cb(data);
            }
        }

        this._socket = new WebSocket(this._url);
        this._socket.onerror    = _socket_error;
        this._socket.onclose    = _socket_close;
        this._socket.onmessage  = _socket_message;
    }

    WebsocketModel.prototype.addListener = function(callback) {
        /* Registers a new listener function.
         * All registered listeners will be called on each new message, and
         * receive the message data as a JSON object.
         */
        this._listeners.push(callback);
    }

    return WebsocketModel;
});
