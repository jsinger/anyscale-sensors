define(['d3.min'], function() {
    var GaugesView = function(model, element) {
        this._model = model;
        this._element = element;
        this._hostname = '';
        this._sensors = {};

        var _this = this;

        // One-time setup
        var svg = d3.select(this._element).append("svg");

        var color = d3.scale.ordinal()
            .domain(['soundlevel', 'temperature', 'motion', 'light'])
            .range(['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728']);

        // The scale maps coordinates of -1 to 1 to pixel values within the available width and height.
        var scale = d3.scale.linear().domain([-1, 1]);

        // The angle scales are different for each sensor, they map the expected value domain to a full rotation.
        var angle_scales = [];
        angle_scales['soundlevel']  = d3.scale.linear().range([0, 2*Math.PI]).domain([0,512]).clamp(true);
        angle_scales['temperature'] = d3.scale.linear().range([-Math.PI, Math.PI]).domain([-40,40]).clamp(true);
        angle_scales['motion']      = d3.scale.linear().range([0, 2*Math.PI]).domain([0,1]).clamp(true);
        angle_scales['light']       = d3.scale.linear().range([0, 2*Math.PI]).domain([0,1000]).clamp(true);

        // values are formatted as 0.00
        var format = d3.format('.2f');

        // Define the arc generator
        var arc = d3.svg.arc()
            .innerRadius(function(d) { return scale(-0.5); })
            .outerRadius(function(d) { return scale(-0.3); })
            .startAngle(0)
            .endAngle(function(d) {
                var angle = angle_scales[d.sensor_type];
                if (angle == undefined)
                    angle = angle_scales[0];
                return angle(d.value);
            });

        var render = function() {
            var num_sensors = d3.keys(_this._sensors).length;
            var width = parseInt(svg.style('width'), 10);
            scale.range([0, width / num_sensors]);

            // Ensure sensors are always sorted in the same order
            var data = d3.values(_this._sensors).sort(function (d1, d2) {
                if (d1.sensor_type < d2.sensor_type)
                    return -1;
                if (d1.sensor_type > d2.sensor_type)
                    return 1;
                return 0;
            });

            // Join data to a group per sensor, check for identity based on the type and hostname.
            // The hostname check is needed because I think d3 internally keeps a reference to the array
            // and does not replace it when it is deemed the same by the key function..
            var g = svg.selectAll('g').data(data, function(d) {
                return d.hostname + "-" + d.sensor_type;
            });

            // When a new sensor is added for the first time, create the group,
            // text nodes, and arc path.
            var enter_selection = g.enter().append('g');

            enter_selection.append('text')
                .text(function(d, i) { return d.sensor_type; })
                .attr('fill', function(d, i) { return color(d.sensor_type); })
                .attr('text-anchor', 'middle')
                .attr('class', 'gauge-type');

            enter_selection.append('text')
                .attr('fill', function(d, i) { return color(d.sensor_type); })
                .attr('text-anchor', 'middle')
                .attr('class', 'gauge-value');

            enter_selection.append("path")
                .attr("fill", function(d, i) { return color(d.sensor_type); });

            // When a sensor type is removed, just delete the group.
            g.exit().remove();

            // On every render, update label positions, value text, and the arc path.
            g.selectAll('.gauge-type')
                .attr("x", scale(0))
                .attr("y", scale(0.8));

            g.selectAll('.gauge-value')
                .text(function(d, i) { return format(d.value); })
                .attr("x", scale(0))
                .attr("y", scale(0.05));

            g.selectAll('path')
                .attr("transform", "translate(" + scale(0) + "," + scale(0) + ")")
                .attr("d", function(d, i) { return arc(d, i); });

            // Evenly distribute the groups
            g.attr('transform', function(d, i) {
                return "translate(" + i * width / num_sensors + ")";
            });

            // Render at a regular interval, align with a frame edge.
            setTimeout(function() {requestAnimationFrame(render);}, 120);
        };

        render();
    };

    // Replace the current value for the given sensor type.
    GaugesView.prototype.update = function(sensor_type, date, value) {
        if (this._sensors[sensor_type] === undefined) {
            this._sensors[sensor_type] = {
                hostname: this._hostname,
                sensor_type: sensor_type,
                date: date,
                value: value
            };
        } else {
            if (this._sensors[sensor_type].date < date) {
                this._sensors[sensor_type].date = date;
                this._sensors[sensor_type].value = value;
            }
        }
    };

    GaugesView.prototype.reset = function(hostname) {
        this._hostname = hostname;

        for (sensor_type in this._sensors) {
            delete this._sensors[sensor_type];
        }

        var _this = this;

        this._model.getNode(this._hostname, function(data) {
            // ignore errors, or when the hostname has changed since the request was dispatched
            if (!data || data.name != _this._hostname) return;

            // Update the current value for each reading contained in the top level node list.
            // may be optimized by relying on the ordering and only taking the latest one...
            for (var sensor of data.sensors) {
                for (var r of sensor.readings) {
                    _this.update(r.sensor_type, new Date(r.timestamp * 1000), r.value);
                }
            }
        });
    };

    return GaugesView;
});
