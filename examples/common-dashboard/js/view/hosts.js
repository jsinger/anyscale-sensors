define(['d3.min'], function() {
    var HostsView = function(model, element) {
        // Options, defaults may be changed
        this.refresh_delay = 30;
        this.offline_delay = 30;

        // Internal state
        this._model = model;
        this._element = element;
        this._last_refresh = 0;
        this._hosts = [];
        this._listeners = [];
        this._selected_name = "";
        this._selected_period = 2*60;
        this._periods = [
            { name: '2min', seconds: 2 * 60 },
            { name: '10min', seconds: 10 * 60 },
            { name: '1h', seconds: 60 * 60 },
            { name: '12h', seconds: 12 * 60 * 60 },
            // longer periods are impractical with current datamodel and requesting _all_ readings in period.
            //{ name: '24h', seconds: 24 * 60 * 60 },
            //{ name: 'week', seconds: 7 * 24 * 60 * 60 }
        ]

        // fix javascript scope issues with event handlers
        var _this = this;

        // Periodically requests a new nodes list and updates the view.
        var refresh = function() {
            _this._model.getNodes(function(nodes){
                if (nodes) {
                    _this._hosts = nodes.nodes.sort(function(a, b) {
                        if (a.name < b.name)
                            return -1;
                        if (a.name > b.name)
                            return 1;
                        else
                            return 0;
                    });
                    _this._last_refresh = nodes.server_timestamp;
                }
                setTimeout(refresh, _this.refresh_delay * 1000);
            });
        }

        // Draws the view with updated data.
        var render = function() {
            // Bind the list of hosts to the list items, matching by name.
            var li = _this._ul.selectAll(".node")
                .data(_this._hosts, function(d) { return d.name; });

            // fill in the names and event handlers when a host is seen for the first time.
            li.enter().append('li')
                .classed('node', true)
                .text(function(d) { return d.name; })
                .attr("title", function(d) { return d.meta.location; })
                .on('click', function(d) {
                    _this._selected_name = d.name;
                    notify_listeners();
                });
            
            // remove any list items where the hosts are not present anymore
            li.exit().remove();

            // Update the presence of the 'alive' class on every refresh, if the last
            // heartbeat is not more than N seconds ago compared to the server clock.
            li.classed('alive', function(d) {
                var diff = _this._last_refresh - d.heartbeat_timestamp;
                return diff <= _this.offline_delay;
            });

            li.classed('selected', function(d) {
                return d.name == _this._selected_name;
            });

            // Enter period selection li
            var period = _this._ul.selectAll(".period")
                .data(_this._periods);

            period.enter().append('li')
                .classed('period', true)
                .text(function(d) { return d.name; })
                .on('click', function(d) {
                    _this._selected_period = d.seconds;
                    notify_listeners();
                });

            period.classed('selected', function(d) {
                return d.seconds == _this._selected_period;
            });

            // render again to update selection
            setTimeout(function() { requestAnimationFrame(render); }, 100);
        }

        // Lets registered listeners know that the selected host name has changed.
        var notify_listeners = function() {
            for (var cb of _this._listeners) {
                cb(_this._selected_name, _this._selected_period);
            }
        }

        // Initialization
        _this._ul = d3.select(_this._element).append("ul");
        refresh();
        render();
    }

    /* Registers a new listener function.
     */
    HostsView.prototype.addListener = function(callback) {
        this._listeners.push(callback);
    }

    return HostsView;
});
