define(['d3.min'], function() {
    var TimelineView = function(model, element) {
        this.view_domain = 300; // how many seconds are displayed
        this._model = model;
        this._element = element;
        this._hostname = '';
        this._sensors = {};

        var _this = this;

        // TODO initially or on changing view-domain, load data from restapi

        // One-time setup
        var svg = d3.select(this._element).append("svg");
        var g_axis = svg.append('g').attr('class', 'axis');

        var color = d3.scale.ordinal()
            .domain(['soundlevel', 'temperature', 'motion', 'light'])
            .range(['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728']);

        // The x-axis uses a time scale mapping to the available width.
        var x = d3.time.scale.utc().clamp(true);

        // The y-axis uses different scales for the different sensor types, mapping to available height.
        var ys = [];
        ys['soundlevel']  = d3.scale.linear().domain([0,1024]).clamp(true);
        ys['temperature'] = d3.scale.linear().domain([-10,40]).clamp(true);
        ys['motion']      = d3.scale.linear().domain([0,10]).clamp(true);
        ys['light']       = d3.scale.linear().domain([0,1000]).clamp(true);

        var y = function(d) {
            var scale = ys[d.sensor_type];
            if (scale == undefined)
                scale = ys[0];
            return scale(d.value);
        };

        // define the path generator
        var line = d3.svg.line()
                .x(function(d) { return x(d.date); })
                .y(function(d) { return y(d); })
                .interpolate("step-before");

        // TODO tick labels, axis, grids
        var axis = d3.svg.axis()
                .scale(x)
                .orient('bottom')
                .outerTickSize(4);

        var render = function() {
            // Save some environment state for calculations
            // Server returns timestamps as UTC seconds.
            var date = new Date();
            var start_date = new Date(date - 1000 * _this.view_domain);
            var width = parseInt(svg.style('width'), 10);
            var height = parseInt(svg.style('height'), 10);

            // The x (time) scale ranges from 0 to width, and its domain changes with the current time.
            x.range([0, width]).domain([start_date, date]);

            // The y scale ranges from height of the element (bottom) to 0 (top)
            for (var scale of d3.values(ys)) {
                scale.range([height, 0]);
            }

            // Join data to a group per sensor, check for identity based on the type.
            // filter data based on dates
            var data = d3.values(_this._sensors);

            // Join the data to dom element
            // replace the stored reference when the hostname, domain, or sensor type is different.
            var g = svg.selectAll('g.data')
                    .data(data, function(d) {
                        return d.hostname + "-" + d.view_domain + "-" + d.sensor_type;
                    });

            // Entering sensor groups are added and leaving ones removed
            var enter = g.enter().append('g')
                .attr('class', 'data')
                .attr("stroke", function(d, i) { return color(d.sensor_type); });

            enter.append("path")
                .attr("class", "line");

            enter.append("path")
                .attr("class", "extrapolated");

            g.exit().remove();

            // Updated on every render/update:
            // generate the paths
            svg.selectAll('.line')
                .attr("d", function(d) { return line(d.values); });

            // Extrapolate the last recorded reading (last element of list), by
            // making a copy of it and setting the date to the current date.
            svg.selectAll('.extrapolated')
                .attr("d", function(d) {
                    var extrapolated = Object.assign({}, d.last);
                    extrapolated.date = date;
                    return line([d.last, extrapolated]);
                 });

            // Draw the grid / axis and labels
            g_axis.call(axis);

            // render again soon; regularly because not every update is also a worth rendering.
            setTimeout(function() {requestAnimationFrame(render);}, 120);
        };

        render();
    };

    TimelineView.prototype.update = function(sensor_type, date, value) {
        var reading = {
            sensor_type: sensor_type,
            date: date,
            value: value
        };

        if (this._sensors[sensor_type] === undefined) {
            this._sensors[sensor_type] = {
                hostname: this._hostname,
                view_domain: this.view_domain,
                sensor_type: sensor_type,
                values: [reading],
                first: reading,
                last: reading
            };
        } else {
            var sensor = this._sensors[sensor_type];

            // either append or prepend the new value to the value list.
            // inserting into the middle is not needed because values arrive as continuous streams
            // in the correct order. This ensures that the values array remains sorted.
            if (sensor.first.date > date) {
                sensor.values.unshift(reading);
                sensor.first = reading;
            } else if (sensor.last.date < date) {
                sensor.values.push(reading);
                sensor.last = reading;
            }

            while (this._sensors[sensor_type].values.length > 10000) {
                this._sensors[sensor_type].values.shift();
            }
        }
    };

    TimelineView.prototype.reset = function(hostname, view_domain) {
        this._hostname = hostname;

        for (sensor_type in this._sensors) {
            delete this._sensors[sensor_type];
        }

        if (view_domain !== undefined)
            this.view_domain = parseInt(view_domain);

        var _this = this;

        var fetch_readings = function(data) {
            // stop on error - node_name is only defined in reading entries, not at global level,
            // so it is checked later.
            if (!data) return;

            var sensor_type;
            var date;

            // API returns values in descending order by date, so after this loop,
            // date will hold the earliest date in the data.
            for (var r of data.readings) {
                // cancel if the hostname has changed
                if (r.node_name != _this._hostname) return;

                date = new Date(r.timestamp * 1000);
                sensor_type = r.sensor_type;

                // add value
                _this.update(sensor_type, date, r.value);
            }

            // if needed, fetch even older readings
            if (date > new Date() - _this.view_domain * 1000) {
                _this._model.getReadingsBefore(_this._hostname, sensor_type, date, fetch_readings);
            }
        };

        var fetch = function(data) {
            // stop on error, or when the hostname has changed since the request was dispatched
            if (!data || data.name != _this._hostname) return;

            // Update the current value for each reading contained in the top level node list.
            // may be optimized by relying on the ordering and only taking the latest one...
            for (var sensor of data.sensors) {
                var date;
                for (var r of sensor.readings) {
                    date = new Date(r.timestamp * 1000);
                    _this.update(r.sensor_type, date, r.value);
                }

                // query more recent readings for this type.
                _this._model.getReadingsBefore(_this._hostname, sensor.sensor_type, date, fetch_readings);
            }
        };

        this._model.getNode(this._hostname, fetch);
    };

    return TimelineView;
});
