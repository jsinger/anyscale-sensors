#!/bin/sh
# 1. Please run this command with root priveleges
# 2. assumption is that packets are getting sent to aukena.dcs.gla.ac.uk ( 130.209.244.250 ) 
#    over an ssh tunnel.We measure at localhost because an ssh tunnel has been set-up to forward to aukena 
# 3. iptables entries are already set up with the required details. see net_traffic_count_en.sh 

#To remove the Aukena Tunnel 
iptables -D OUTPUT 1
iptables -D Aukena_Tunnel 1
iptables -X Aukena_Tunnel 
