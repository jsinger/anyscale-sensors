#!/bin/sh
# 1. Please run this command with sudo 
# 2. assumption is that packets are getting sent to aukena.dcs.gla.ac.uk ( 130.209.244.250 ) 
#    over an ssh tunnel.We measure at localhost because an ssh tunnel has been set-up to forward to aukena 

iptables -N Aukena_Tunnel
iptables -A Aukena_Tunnel -j ACCEPT
iptables -A OUTPUT -d 130.209.244.250 -p tcp --dport 22 -j Aukena_Tunnel
