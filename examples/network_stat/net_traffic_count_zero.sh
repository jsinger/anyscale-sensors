#!/bin/sh
# Prints the current counter values for packets, bytes, and resets them to zero.

# 1. Please run this command with sudo 
# 2. assumption is that packets are getting sent to aukena.dcs.gla.ac.uk ( 130.209.244.250 ) 
#    over an ssh tunnel.We measure at localhost because an ssh tunnel has been set-up to forward to aukena 
# 3. iptables entries are already set up with the required details. see net_traffic_count_en.sh 

# view packet counts and network data transferred 
STATS=$(iptables -L Aukena_Tunnel -nv | awk '/ACCEPT/ {print $1, $2}' | sed -e "s/ /, /")
echo "`date`, $STATS"

# zero out the counters on our Aukena_Tunnel Chain: 
iptables -Z Aukena_Tunnel 1
