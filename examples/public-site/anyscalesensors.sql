-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 07, 2016 at 09:56 AM
-- Server version: 5.5.49
-- PHP Version: 5.3.10-1ubuntu3.23

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `anyscalesensors`
--

-- --------------------------------------------------------

--
-- Table structure for table `hosts`
--

CREATE TABLE IF NOT EXISTS `hosts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(64) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(256) NOT NULL,
  `slug` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `hosts`
--

INSERT INTO `hosts` (`id`, `hostname`, `name`, `description`, `slug`) VALUES
(1, 'pi1', 'F101 (pi1)', 'This sensor is currently being used for demonstration purposes and redeployed frequently.', 'f101'),
(2, 'pi11', 'Demo (pi11)', 'This sensor is currently being used for demonstration purposes and redeployed frequently.', NULL),
(3, 'pi12', 'Demo (pi12)', 'This sensor is currently being used for demonstration purposes and redeployed frequently.', NULL),
(4, 'pi5', 'pi5', 'This sensor is currently being used for development and testing and redeployed frequently.', NULL),
(5, 'pi8', 'pi8', '', NULL),
(6, 'pi9', 'pi9', '', NULL),
(7, 'pi10', 'pi10', '', NULL),
(8, 'pi13', 'pi13', '', NULL),
(9, 'pi14', 'pi14', '', NULL),
(10, 'pi15', 'pi15', '', NULL),
(11, 'pi16', 'pi16', '', NULL),
(12, 'pi4', 'SAWB 421 (pi4)', 'This sensor is currently being used for demonstration purposes and redeployed frequently.', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sensors`
--

CREATE TABLE IF NOT EXISTS `sensors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kind` varchar(32) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `sensors`
--

INSERT INTO `sensors` (`id`, `kind`, `name`, `description`) VALUES
(1, 'dummy', 'Random Number', 'A random number generator, used for testing the sensor data reporting infrastructure.'),
(2, 'temperature', 'Temperature', 'Measures the ambient temperature.'),
(3, 'light', 'Light', 'Measures ambient light levels.'),
(4, 'motion', 'Motion', 'Records an event when movement is detected within its field of view.'),
(5, 'soundlevel', 'Sound Level', 'A sensor measuring the average volume of its surroundings only.');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `hostid` int(11) NOT NULL,
  `sensorid` int(11) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `value` varchar(32) NOT NULL,
  PRIMARY KEY (`hostid`,`sensorid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`hostid`, `sensorid`, `timestamp`, `value`) VALUES
(1, 2, 1463993083, '25.81'),
(1, 3, 1463993123, '21.96'),
(1, 4, 1463993090, 'False'),
(1, 5, 1463993121, '15'),
(2, 2, 1463992945, '22.25'),
(2, 3, 1463993122, '2.77'),
(2, 4, 1463993104, 'True'),
(2, 5, 1463993123, '21'),
(3, 2, 1461689333, '25.75'),
(3, 3, 1461689333, '7.6'),
(3, 4, 1461689333, 'False'),
(3, 5, 1461689333, '26'),
(8, 2, 1461682387, '23.0'),
(8, 3, 1461682391, '20.3'),
(8, 4, 1461682387, 'True'),
(9, 2, 1461682388, '23.19'),
(9, 3, 1461682392, '5.96'),
(9, 4, 1461682393, 'False'),
(10, 2, 1461682387, '23.94'),
(10, 3, 1461682391, '25.51'),
(10, 4, 1461682388, 'False'),
(11, 2, 1461682387, '25.31'),
(11, 3, 1461682391, '20.11'),
(11, 4, 1461682392, 'True'),
(12, 2, 1463992947, '22.31'),
(12, 3, 1463993119, '9.97'),
(12, 4, 1463993089, 'True');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
