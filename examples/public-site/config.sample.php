<?php
  /* Copy to config.php and fill in the blanks. */

  /* The SECRET_KEY is a shared secret string. It is used to generate a signature from the posted data
  in the upload script and on the server. */
  define('SECRET_KEY', '');

  /* MYSQL login details */
  define('DB_HOST', 'localhost');
  define('DB_DATABASE', '');
  define('DB_USER', '');
  define('DB_PASSWORD', '');
?>
