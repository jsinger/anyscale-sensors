<?php

/* Just include the template, trusting that it includes header, footer etc. */
function get_template($template) {
    include('templates/'.$template.'.html.php');
}

/* generate a key-sorted query string representation of the given associative array. */
function serialize_request($assoc) {
    $keys = array_keys($assoc);
    sort($keys);

    $list = array();
    foreach($keys as $k) {
        $list[] = $k . "=" . $assoc[$k];
    }

    return implode('&', $list);
}

function verify_key_signature($key, $data, $their_signature) {
    $our_signature = hash_hmac('sha256', $data, $key);
    print "theirs: $their_signature\nours: $our_signature";
    if (function_exists('hash_equals')) {
        return hash_equals($our_signature, $their_signature);
    } else {
        return $our_signature === $their_signature;
    }
}

function push_data($assoc) {
    global $db;

    $host_ids = array();
    $sensor_ids = array();

    foreach($assoc as $key => $value) {
        # key is of the form hostname/sensorkind
        $key_parts = explode('/', $key);
        $host_name = $key_parts[0];
        $sensor_kind = $key_parts[1];

        # value is of the form timestamp,value
        $value_parts = explode(',', $value);
        $timestamp = intval($value_parts[0]);
        $value = $value_parts[1];

        if (!isset($host_ids[$host_name])) {
            $result = $db->query('SELECT id FROM hosts WHERE hostname = \''.$db->escape_string($host_name).'\'');
            if($result->num_rows === 1) {
                $row = $result->fetch_assoc();
                $host_ids[$host_name] = intval($row['id']);
            } else {
                # drop if invalid host given
                error_log("host name not found in database: $host_name");
                $result->free();
                continue;
            }
            $result->free();
        }

        if (!isset($sensor_ids[$sensor_kind])) {
            $result = $db->query('SELECT id FROM sensors WHERE kind = \''.$db->escape_string($sensor_kind).'\'');
            if($result->num_rows === 1) {
                $row = $result->fetch_assoc();
                $sensor_ids[$sensor_kind] = intval($row['id']);
            } else {
                # silently drop this entry if invalid sensor kind given
                error_log("sensor kind not found in database: $sensor_kind");
                $result->free();
                continue;
            }
            $result->free();
        }

        $db->query('INSERT INTO status (hostid, sensorid, value, timestamp)
            VALUES (\''.$host_ids[$host_name].'\', \''.$sensor_ids[$sensor_kind].'\', \''.$db->escape_string($value).'\', \''.$timestamp.'\')
            ON DUPLICATE KEY UPDATE value = VALUES(value), timestamp = VALUES(timestamp)');
    }
}

?>
