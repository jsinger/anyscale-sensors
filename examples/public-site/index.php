<?php
include('config.php');
include('functions.php');

$db = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
if ($db->connect_errno) {
    echo "Failed to connect to MySQL: " . $db->connect_error;
    die();
}

switch($_GET['action']) {
case 'push':
    if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
        header('HTTP/1.0 404');
        get_template('404');
        die();
    }

    # TODO authenticate the request
    $serialized_data = serialize_request($_POST);
    $signature = $_GET['signature'];

    if (verify_key_signature(SECRET_KEY, $serialized_data, $signature) !== true) {
        die('unauthorized');
    } else {
        push_data($_POST);
    }

    break;
case 'host':
    $where = '';
    $hostid = 0;

    if (array_key_exists('id', $_GET)) {
        $hostid = intval($_GET['id']);
        $where = "id = '".$hostid."'";
    } else if (array_key_exists('slug', $_GET)) {
        $hostslug = $db->real_escape_string($_GET['slug']);
        $where = "slug = '".$hostslug."'";
    } else {
        die("missing id or slug parameter");
    }

    # Retrieve host information
    $result = $db->query('SELECT id, name, description FROM hosts
        WHERE '.$where);

    if (!$result || $result->num_rows !== 1) {
        header('HTTP/1.0 404');
        get_template('404');
        die();
    }

    $host = $result->fetch_assoc();
    $result->free();

    # Retrieve the sensor readings
    $result = $db->query('SELECT timestamp, kind, value, name, description
        FROM status st, sensors se
        WHERE st.sensorid = se.id AND st.hostid=\''.intval($host['id']).'\'');

    $sensors = array();

    while ($s = $result->fetch_assoc()) {
        $sensors[$s['kind']] = $s;
    }
    $result->free();

    # Include the host template
    get_template('host');
    break;
case 'index':
    # List of hosts
    $result = $db->query('SELECT id, name FROM hosts ORDER BY name ASC');

    $hosts = array();
    while ($h = $result->fetch_assoc()) {
        $hosts[] = $h;
    }
    $result->free();

    get_template('index');
    break;
case 'labels':
    # List of hosts
    $result = $db->query('SELECT * FROM hosts ORDER BY id ASC');

    $hosts = array();
    while ($h = $result->fetch_assoc()) {
        $hosts[] = $h;
    }
    $result->free();

    get_template('labels');
    break;
default:
    die('action not implemented');
}

?>
