<!DOCTYPE html>
<html>
<head>
    <title>Page not found</title>
    <?php get_template('head'); ?>
</head>
<body>
<?php get_template('header'); ?>

<h1>Page not found.</h1>
<p>Oops! There's nothing here.</p>
<p>You may have followed an outdated link.</p>
<p><a href="/index.html">Please see here for an overview of our project</a>.</p>

<?php get_template('footer'); ?>
</body>
</html>
