<?php
    global $host, $sensors;
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo htmlspecialchars($host['name']); ?></title>
    <?php get_template('head'); ?>
</head>
<body>
<?php get_template('header'); ?>

<div class="container-host">
<h1><?php echo htmlspecialchars($host['name']); ?></h1>

<p class="description"><?php
    echo htmlspecialchars($host['description']);
?></p>
</div>

<div class="container-data">
<h2>Latest Data</h2>

<ul class="list-sensors">
<?php foreach($sensors as $kind => $sensor): ?>
    <li class="item-sensor <?php echo htmlspecialchars($kind); ?>">
        <h3 class="sensor-kind"><?php
            echo htmlspecialchars($sensor['name']);
        ?></h3>
        <p class="sensor-value"><?php
            echo htmlspecialchars($sensor['value']);
        ?></p>
        <p class="sensor-time">
            <time class="timeago" datetime="<?php echo date('c', $sensor['timestamp']); ?>"><?php
                echo date('j M Y H:i:s', $sensor['timestamp']);
            ?></time>
        </p>
        <p class="sensor-description"><?php
            echo htmlspecialchars($sensor['description']);
        ?></p>
    </li>
<?php endforeach; ?>
</ul>
</div>

<div class="container-about">
    <h2>General Information</h2>
    <p>This device is part of an environmental sensing project
in the School of Computing Science.</p>

    <p>We are measuring room temperature, light, and volume levels.
    We are also measuring whether anything is moving in this room.
    No personal data is recorded or stored on this system.</p>

    <p>The environmental data is stored in a database in the School of
    Computing Science.</p>

    <p>The device is based on a Raspberry Pi with commodity electronic
    sensors attached.</p>

    <p>For more information about the project, please contact <a href="mailto:Jeremy.Singer@glasgow.ac.uk">Jeremy Singer</a> or <a href="mailto:Matthew.Chalmers@glasgow.ac.uk">Matthew Chalmers</a>.</p>
</div>

<?php get_template('footer'); ?>
</body>
</html>
