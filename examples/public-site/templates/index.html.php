<?php global $hosts; ?>
<!DOCTYPE html>
<html>
<head>
    <title>Home</title>
    <?php get_template('head'); ?>
</head>
<body>
<?php get_template('header'); ?>

<h1>Home</h1>

<p>A number of Raspberry Pi devices have been deployed around the School of Computing Science. This website allows you to access recent environmental sensor readings collected by them.</p>

<ul class="hosts">
<?php foreach($hosts as $host): ?>
    <li><a href="/<?php echo intval($host['id']); ?>"><?php echo htmlspecialchars($host['name']); ?></a></li>
<?php endforeach; ?>
</ul>

<?php get_template('footer'); ?>
</body>
</html>
