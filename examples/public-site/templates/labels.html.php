<?php global $hosts; ?>
<!DOCTYPE html>
<html>
<head>
    <title>Sensor box labels</title>
    <style type="text/css">
     body {
         font-family:sans-serif;
     }
     .label {
         page-break-inside: avoid;
         padding: 0.25cm;
         padding-left: 3.5cm;
         position: relative;
     }
     .label-qrcode {
         width: 3cm;
         height: 3cm;
         position:absolute;
         left: 0.25cm;
         top: 0.25cm;
     }
     .label-url {
font-weight: bold;
     }
     .label-hostname{
         font-size: 0.5em;
         position: absolute;
         top: 0;
         right: 0;
     }
     h1 {
         margin: 0;
     }
    </style>
</head>
<body>
<?php get_template('header'); ?>

<?php foreach($hosts as $host): ?>
<div class="label">
    <h1>Raspberry Pi Sensors Project</h1>
    <p>This device is part of an environmental sensing project in the School of Computing Science. This is a prototype for Glasgow's new Smart Campus infrastructure.</p>
    <p>Check the link below to see this sensor's data.</p>
    <p>For more information, please contact Jeremy Singer or Matthew Chalmers.</p>
    <p class="label-url">http://sensors.anyscale.org/<?php echo htmlspecialchars($host['id']); ?></p>
    <img src="http://api.qrserver.com/v1/create-qr-code/?color=000000&bgcolor=FFFFFF&data=http%3A%2F%2Fsensors.anyscale.org%2F<?php echo htmlspecialchars($host['id']); ?>&qzone=1&margin=0&size=400x400&ecc=L" class="label-qrcode">
    <p class="label-hostname"><?php echo htmlspecialchars($host['hostname']); ?></p>
</div>
<?php endforeach; ?>

<?php get_template('footer'); ?>
</body>
</html>
