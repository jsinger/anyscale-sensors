# blink

The `blink` service flashes an LED connected to a GPIO pin while it is running.

`blink` is the _hello-world_ of our GPIO services and primarily used during development of the deployment and remote management infrastructure.

## Configuration

The service configuration file, `/to/be/decided/blink.ini`, specifies the flashing pattern by giving the durations that the LED should be on and off in seconds. It also allows to change the GPIO pin connected to the LED.

```
[blink]
led_pin = 17
duration_on = 0.3
duration_off = 0.7
```

## Implementation

The `blink` service is implemented as a python script using the `RPi.GPIO` module for accessing the GPIO pin.
