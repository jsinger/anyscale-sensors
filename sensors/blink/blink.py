#!/usr/bin/python
import RPi.GPIO as GPIO
import time
import os
import ConfigParser

# Define configuration file path
config_file = '/root/anyscale-sensors/blink.conf'
led_pin = 17       # The BCM pin number for the LED GPIO pin
duration_on = 0.1  # how long to stay on (and off) for, times are
duration_off = 0.1 # given in (floating point) seconds

# Default definitions, these may be overridden by the configuration file
default_options = {
    'led_pin'      : str(led_pin),
    'duration_on'  : str(duration_on),
    'duration_off' : str(duration_off)
}

try:
    # Read from the config file, will silently fall back to defaults if it cannot be parsed
    options = ConfigParser.SafeConfigParser(default_options)
    options.read([config_file])

    # assign the options that are actually used
    led_pin      = options.getint('blink', 'led_pin')
    duration_on  = options.getfloat('blink', 'duration_on')
    duration_off = options.getfloat('blink', 'duration_off')
except Exception as e:
    print "Invalid configuration:", e.message
    exit(1)

# Wrap everything touching the GPIO in a try ... finally block to ensure we
# always release the GPIO when the process is killed or fails for some reason.
try:
    # Select pin numbering mode as Broadcom physical pin numbers
    GPIO.setmode(GPIO.BCM)

    # Set up the LED pin as an output
    GPIO.setup(led_pin, GPIO.OUT)

    # Blink forever at the given durations
    while True:
        GPIO.output(led_pin, GPIO.LOW)
        time.sleep(duration_off)
        GPIO.output(led_pin, GPIO.HIGH)
        time.sleep(duration_on)
except Exception as e:
    print e.message
    exit(1)
finally:
    GPIO.cleanup()
