#!/bin/sh
mkdir -p /root/anyscale-sensors/
cp blink.py /root/anyscale-sensors/
cp blink.conf /root/anyscale-sensors/
cp anyscale-sensors.blink.service /lib/systemd/system/
systemctl enable anyscale-sensors.blink
service anyscale-sensors.blink restart
