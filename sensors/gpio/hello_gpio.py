#!/usr/bin/python
import RPi.GPIO as GPIO
import time

ledPin = 17
sensePin = 22

def handleEdge(pin):
    if (GPIO.input(sensePin)):
        print 'UP'
        GPIO.output(ledPin, GPIO.HIGH)
    else:
        print 'DOWN'
        GPIO.output(ledPin, GPIO.LOW)

try:
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(ledPin, GPIO.OUT)
    GPIO.setup(sensePin, GPIO.IN)

    GPIO.add_event_detect(sensePin, GPIO.BOTH)
    GPIO.add_event_callback(sensePin, handleEdge)

    while True:
        time.sleep(1)
        print '.',
finally:
    GPIO.cleanup()
