# I2C Sensors

There are a number of sensors that communicate via the _i2c_ bus. They are grouped in this folder as they will likely require a single script to manage the bus and communicate with all of them. The sensors currently planned are:

- Temperature sensor TMP102, Address `0x48`
- Luminosity sensor TSL2561, Address `0x39`

## System setup

The I2C interface needs to be enabled on the Pi. For Raspbian, just install `i2c-tools` and run `raspi-config` to enable the I2C interface in advanced options.

The python scripts currently use the `smbus` library to communicate with the I2C bus. Install it as `apt-get install python-smbus`.
