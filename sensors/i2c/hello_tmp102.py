#!/usr/bin/python
import smbus
import time

device_address = 0x48
interval = 1.0

# Initialise I2C bus 0
b = smbus.SMBus(0)

def swap_word_bytes(word):
    lsb = (word & 0xFF00) >> 8
    msb = word & 0xFF
    return (msb << 8) | lsb

# Forever print the current temperature
while True:
    data = swap_word_bytes(b.read_word_data(device_address, 0x00))
    temperature = (data >> 4) * 0.0625
    print temperature, " C"
    time.sleep(interval)
