#!/usr/bin/python
import smbus
import time

device_address = 0x39
interval = 1.0

# Initialise I2C bus 0
b = smbus.SMBus(0)

# Power up the light sensor
b.write_byte_data(device_address, 0x0, 0x03);

# set timing register to use scale factor 1 (402ms integration time)
b.write_byte_data(device_address, 0x81, 0b00000010)

# Wait at least 402ms to ensure the sensor is powered up and the first conversion is complete.
time.sleep(0.5)

# Convert to lux using coefficients for T/FN/CL package, ch0 and ch1 are integers
def data_to_lux(ch0, ch1):
    lux = 0

    if (ch0 == 0):
        return lux

    ratio = 1.0 * ch1/ch0

    print ch1, '/', ch0, 'ratio =', ratio

    if ratio < 0.50:
        lux = 0.0304 * ch0 - 0.062 * ch0 * (ratio ** 1.4)
    elif ratio < 0.61:
        lux = 0.0224 * ch0 - 0.031 * ch1
    elif ratio < 0.80:
        lux = 0.0128 * ch0 - 0.0153 * ch1
    elif ratio < 1.30:
        lux = 0.00146 * ch0 - 0.00112 * ch1
    else: #ratio > 1.30
        Lux = 0

    return lux

# Forever print the current light level
cmd = 0b10100000 # select command register, select word mode


while True:
    ch0 = b.read_word_data(device_address, cmd | 0xC)
    ch1 = b.read_word_data(device_address, cmd | 0xE)

    print data_to_lux(ch0, ch1), "lux"

    time.sleep(interval)
