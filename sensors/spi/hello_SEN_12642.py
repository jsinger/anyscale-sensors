import spidev
import numpy    # Only require the median function
import time     # Only require the sleep function

_SPI_DEVICE = 0
_SPI_CHANNEL = 0
_SPI_SPEED_HZ = 3000000

_SOUND_SENSOR_SAMPLING_HZ_ = 20        #How many (times/second) should we sample
_SOUND_SENSOR_SAMPLING_PERIOD_ = 1     #Total time to sample sound


class SoundlevelSensor():
    """ This class is not thread safe. 
        Relies on user to make it thread safe
    """ 
    def __init__(self, freq = _SOUND_SENSOR_SAMPLING_HZ_ , period = _SOUND_SENSOR_SAMPLING_PERIOD_):
        self.sound_samples = []
        self.sample_freq = freq 
        self.sample_period = period 


    def __open_spi_bus__(self):
        """ Internal class function to set-up SPI channel parameters 
            and to open handle to SPI bus
            Return Value : A handle to requisite spi bus 
        """    
        self.spi = spidev.SpiDev()
        self.to_send = [0xd0, 0x00, 0x00]

        try:
            self.spi.open(_SPI_DEVICE, _SPI_CHANNEL)
        except Exception:
            return None
        finally:
            return None


    def __close_spi_bus__(self):
        """ Internal class function to close SPI channel previously opened
            Return Value : None
        """    
        try:
            self.spi.close()
        except Exception:
            return None
        finally:
            return None

    def __get_instantaneous_value__(self):
        """ Internal class function to SPI channel parameters 
            and to open handle to SPI bus
            Return Value : A handle to requisite spi bus 
        """    
        try:
            bytes = self.spi.xfer(self.to_send)
            return self._adc_value(bytes)
        except Exception:
            return None

    def get_single_sensor_value(self):
        """ Function returns median of samples observed over a time period
            Return Value : A single instantaneaous sample from the Sound Envelope sensor
            Notes        : Function internally
                           i)   opens spi bus , obtains 
                           ii)  gets instantaneaous sound envelope value and 
                           iii) closes spi-bus
        """
        try:
            self.__open_spi_bus__()
            value =  self.__get_instantaneous_value__()
            self.__close_spi_bus__()
            return value 
        except Exception:
            return None

    def get_sensor_value(self):
        """ Function returns median of samples observed over a time period
            Return Value : tuple (x,y) 
                               x : median of all samples within specified period
                               y : array of all instantaneaous samples within
                                   specified period
            Notes        : Function internally 
                           i)   opens spi bus , obtains 
                           ii)  samples at frequency of 'sample_freq' 
                                over a period of  'sample_preiod'  and 
                           iii) closes spi-bus
        """
        try:
            sleep_time = (1.0/self.sample_freq)
            no_of_samples = (self.sample_period / sleep_time)
            samples = []

            self.__open_spi_bus__()
            for idx in range(0, int(no_of_samples) ):
                samples.append( self.__get_instantaneous_value__() )
                time.sleep( sleep_time )

            self.__close_spi_bus__()
            return ( numpy.median(samples), samples )     # return a tuple containing the median of the samples, and the instantaneous sampled values
        except Exception:
            return None


    def _adc_value(self, bytes):
        high = bytes[0] & 0b0111
        low = bytes[1] >> 1
        return high << 7 | low

def main():
    sensor = SoundlevelSensor()
    (mean,obs) = sensor.get_sensor_value()
    print 'Sensor Value : Mean = {0:3.2f} \nInstantaneous Samples = {1}'.format(mean,obs)



if (__name__ == "__main__"):
    main()
