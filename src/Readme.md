# Project Source

This folder contains the source code for all the different applications developed for this project: Servers, clients, and utilities. Note that some applications may depend on available third-party services, such as a database. This will be documented with the respective application source.

## Python Applications Source Code

`pkg/` contains the python modules making up the individual applications.

## Protocol Definitions

`protos/` contains definitions of the services and message types.

## Run Scripts

`./run_*.sh`

These scripts should be invoked from the src folder. They will set up the required default options to the python modules making up the individual applications

## Requirements

The python implementations rely on a correctly set up environment. This can be achieved by using a `virtualenv` wrapper:

```sh
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

Additionally, on the Raspberry Pi nodes that run sensor services, the I2C (python-smbus) and GPIO module must be installed.

**TODO**: Check whether installation of these system-wide modules before initializing the virtual environment is sufficient, or if symlinks need to be created manually.

**TODO**: Add documentation of the wheel based distribution of precompiled binary packages. On the Raspberry Pi, compilation of C modules takes a long time, this can be avoided by unpacking a binary package created on another Pi using the wheel utility, like `pip install --use-wheel --no-index --find-links=wheels protobuf` when dependency wheel files are stored in the wheels/ folder.
