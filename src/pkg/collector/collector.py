from ..protocols import heartbeat_pb2
from ..protocols import collector_pb2
from ..protocols import measurements_pb2
from ..common import krpc as implementations
from ..common import config
from threading import Thread, Event
from Queue import Queue, Empty
import os
import time

class CollectorService(collector_pb2.CollectorService):
    """ Implements the collector service interface. The RPC endpoints contained will
    call a registered callback. """
    status_callback = None
    heartbeat_callback = None

    def Report(self, rpc_controller, request, context):
        """ Endpoint for receiving a single measurement from a sensor.
        """

        # call the callback, if any.
        if self.status_callback != None:
            self.status_callback(request)

        # reply with an acknowledgment only.
        return collector_pb2.SensorStatusReply(ack=True)

    def Heartbeat(self, rpc_controller, request, context):
        """ Endpoint for receiving a heartbeat status message from a sensor."""

        # call the callback, if any.
        if self.heartbeat_callback != None:
            self.heartbeat_callback(request.error, request.sensor_type)

        # reply with an acknowledgment only.
        # TODO: Add a sensor configuration message (config) if needed.
        return collector_pb2.HeartbeatReply(ack=True, config=None)

    def SetStatusCallback(self, cb):
        """ Register a callback for when a sensor status is received.
        Overwrites any previous status callback.

        The callback method is given the original sensor status message object.

        """
        self.status_callback = cb

    def SetHeartbeatCallback(self, cb):
        """ Register a callback for when a heartbeat is received by the collector.
        Overwrites any previous heartbeat callback.
        """
        self.heartbeat_callback = cb


class Collector():
    hostname = os.uname()[1] or "unknown"
    q_received = None
    q_send = None
    bundle_event = None
    active_sensor_types = None
    server_timestamp_offset = 0

    measurements = None
    heartbeat = None

    def __init__(self):
        self.q_received = Queue()
        self.q_send = Queue()
        self.bundle_event = Event()
        self.active_sensor_types = set()

    def StatusCallback(self, status):
        """ To be called immediately when a new sensor status is received by the server.
        Adds it to the queue.
        """

        self.active_sensor_types.add(status.sensor_type)
        self.q_received.put(status)

    def HeartbeatCallback(self, error, sensor_type):
        """ To be called when a sensor sends a heartbeat message, including on sensor
        startup. Register its existence.
        """

        print "Heartbeat callback, %s" % sensor_type
        self.active_sensor_types.add(sensor_type)

    def RunBundling(self):
        """ This thread processes the sensor status objects from the received queue,
        bundling them up into measurement report objects periodically. These are
        put on the send queue to be processed by another thread. The bundling
        process is done periodically but can be done immediately, too.
        """

        # TODO hard-coded limit for now. How can I make sure the maximum message
        # size is not exceeded without having to Serialize the message after
        # every addition? The krpc implementation limits the serialized message
        # size to 64K bytes.
        limit = 100

        while True:
            self.bundle_event.wait()
            self.bundle_event.clear()

            # Construct a new Measurements object
            m = measurements_pb2.Measurements()
            m.hostname = self.hostname

            # Add each status from the received-queue, up to the limit.
            count = 0
            for i in range(limit):
                status = None

                # A non-blocking get from the queue, allows us to send a report with
                # less items than the limit in it.
                try:
                    status = self.q_received.get(block=False)
                except Empty:
                    break

                # We make a copy of the status into a newly allocated status
                # object inside the message, in case the original gets re-used
                # somewhere. I don't think this is strictly necessary here.
                datum = m.data.add()
                datum.CopyFrom(status)
                count += 1

                # Adjust sensor timestamp to server clock
                datum.timestamp += self.server_timestamp_offset

            # The compiled message gets put on the send-queue only if there is any data in it.
            if count > 0:
                print "Bundling thread bundled %d items." % count
                self.q_send.put(m)

    def RunSending(self):
        """ This thread processes the bundled measurement reports and sends them in
        order. If sending a report fails, it is retried after a configured interval.
        """
        while True:
            report = self.q_send.get()

            while not self._SendMeasurements(report):
                # TODO more differentiated error handling: The current
                # implementation will end up in a loop of immediate retries
                # until there is a success. So for now I just add a delay to
                # slightly alleviate this.
                print "Failed to send report, retrying after delay."
                time.sleep(config.getfloat('Collector', 'ReportRetryDelay'))

    def _SendMeasurements(self, report):
        """ Sends a measurements report to the measurements server and reports whether
        it was successful.
        """
        controller = implementations.create_controller()
        self.measurements.Report(controller, report, None)
        return not controller.Failed()

    def RunBundleTimer(self):
        """ This thread triggers (sets) the bundle_event at a fixed interval. """
        # TODO this implements a fixed interval, we should also support
        # cancelling this timer when receiving a report-now or
        # change-timeout event.
        while True:
            self.bundle_event.set()
            time.sleep(config.getfloat('Collector', 'ReportPeriod'))

    def RunHeartbeat(self):
        """ This thread sends heartbeat reports at a fixed interval. """

        while True:
            # Initialize a heartbeat message.
            heartbeat = heartbeat_pb2.HeartbeatMessage()

            # Set the hostname and timestamp.
            heartbeat.hostname = self.hostname
            heartbeat.timestamp = int(time.time())
            heartbeat.meta.location = config.get('Meta', 'Location')

            # Add the sensor types registered.
            for sensor_type in self.active_sensor_types:
                heartbeat.sensors.append(sensor_type)

            # Try to send the heartbeat, discard it if it fails.
            if not self._SendHeartbeat(heartbeat):
                print "Failed to send heartbeat. Discarding."
            else:
                print "Sent heartbeat."

            # Sleep for the configured number of seconds, until the next heartbeat is due.
            time.sleep(config.getfloat('Collector', 'HeartbeatPeriod'))

    def _SendHeartbeat(self, heartbeat):
        """ Sends the heartbeat and reports whether it was successful. """
        controller = implementations.create_controller()
        reply = self.heartbeat.Heartbeat(controller, heartbeat, None)

        if reply != None and reply.server_timestamp:
            self.server_timestamp_offset = int(reply.server_timestamp - time.time())

        return not controller.Failed()

    def ConnectHeartbeat(self):
        """ Initialize the connection to the heartbeat service. """
        port = (config.get('Collector', 'HeartbeatHost'), config.get('Heartbeat', 'Port'))
        channel = implementations.insecure_channel(*port)
        stub = heartbeat_pb2.HeartbeatService_Stub(channel)
        return stub

    def ConnectMeasurements(self):
        """ Initialize the connection to the measurements service. """
        port = (config.get('Collector', 'MeasurementsHost'), config.get('Measurements', 'Port'))
        channel = implementations.insecure_channel(*port)
        stub = measurements_pb2.MeasurementsService_Stub(channel)
        return stub

    def Run(self):
        """ Set up the reporting channels and kick off the threads for timing and queue
        management.
        """
        self.heartbeat = self.ConnectHeartbeat()
        self.measurements = self.ConnectMeasurements()

        self.heartbeat_thread = Thread(target = self.RunHeartbeat).start()
        self.sending_thread   = Thread(target = self.RunSending).start()
        self.bundling_thread  = Thread(target = self.RunBundling).start()
        self.timer_thread     = Thread(target = self.RunBundleTimer).start()

def main():
    config.init('collector')

    # Start the collector specific threads.
    collector = Collector()
    collector.Run()

    # Initialise the RPC servicer and add callbacks to the collector.
    servicer = CollectorService()
    servicer.SetStatusCallback(collector.StatusCallback)
    servicer.SetHeartbeatCallback(collector.HeartbeatCallback)

    # Start serving TCP requests to the collector service
    port = '%s:%s' % (config.get('Collector', 'Host'), config.get('Collector', 'Port'))
    print "Collector service listening on %s." % port
    server = implementations.create_server(servicer)
    server.add_insecure_port(port)
    server.serve_forever()

if (__name__ == "__main__"):
    main()
