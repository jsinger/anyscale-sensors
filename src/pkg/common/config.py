import ConfigParser
import os

_CONFIG_DIR = '/etc/anyscale-sensors/'

_PARSER = ConfigParser.SafeConfigParser()

# Default sections and values are created when the module is imported.
# These will be overridden by the system-wide configuration files.
# To ensure no exceptions are triggered when these files do not exist,
# all sections and values, even if they are empty by default, must be listed here.
_PARSER.add_section('Mongo')
_PARSER.set('Mongo', 'Host', 'localhost')
_PARSER.set('Mongo', 'Database', 'AnyscaleSensors')
_PARSER.set('Mongo', 'Port',  '27017')

_PARSER.add_section('Redis')
_PARSER.set('Redis', 'Host', 'localhost')
_PARSER.set('Redis', 'Port',  '6379')

_PARSER.add_section('Collector')
_PARSER.set('Collector', 'Host', 'localhost')
_PARSER.set('Collector', 'Port', '50020')
_PARSER.set('Collector', 'HeartbeatHost', 'localhost')
_PARSER.set('Collector', 'MeasurementsHost', 'localhost')
_PARSER.set('Collector', 'ReportPeriod', '30')
_PARSER.set('Collector', 'ReportRetryDelay', '10')
_PARSER.set('Collector', 'HeartbeatPeriod', '10')

_PARSER.add_section('Heartbeat')
_PARSER.set('Heartbeat', 'Host', 'localhost')
_PARSER.set('Heartbeat', 'Port', '50030')

_PARSER.add_section('Measurements')
_PARSER.set('Measurements', 'Host', 'localhost')
_PARSER.set('Measurements', 'Port', '50040')

_PARSER.add_section('Restapi')
_PARSER.set('Restapi', 'Host', 'localhost')
_PARSER.set('Restapi', 'Port', '5000')
_PARSER.set('Restapi', 'Debug', 'True')

_PARSER.add_section('Sensor')
_PARSER.set('Sensor', 'CollectorHost', 'localhost')
_PARSER.set('Sensor', 'ReportUnchanged', 'False')
_PARSER.set('Sensor', 'HeartbeatPeriod', '15')
_PARSER.set('Sensor', 'ReportPeriod', '5')

_PARSER.add_section('I2c')
_PARSER.set('I2c', 'Host', 'localhost')
_PARSER.set('I2c', 'Port', '50010')
_PARSER.set('I2c', 'BusNumber', '0')

_PARSER.add_section('DummySensor')

_PARSER.add_section('LightSensor')
_PARSER.set('LightSensor', 'I2cHost', 'localhost')

_PARSER.add_section('TemperatureSensor')
_PARSER.set('TemperatureSensor', 'I2cHost', 'localhost')

_PARSER.add_section('MotionSensor')
_PARSER.set('MotionSensor', 'SensorPin', '22')

_PARSER.add_section('SoundlevelSensor')
_PARSER.set('SoundlevelSensor', 'SamplingPeriod', '1')
_PARSER.set('SoundlevelSensor', 'SamplingFrequency', '20')

_PARSER.add_section('Websockets')
_PARSER.set('Websockets', 'Port', '5001')
_PARSER.set('Websockets', 'Host', 'localhost')

_PARSER.add_section('Mqtt')
_PARSER.set('Mqtt', 'Hostname', 'localhost')
_PARSER.set('Mqtt', 'Port', '1883')
_PARSER.set('Mqtt', 'AuthToken', 'SecretToken')
_PARSER.set('Mqtt', 'ClientId', 'g:orgid:typeid:deviceid')

_PARSER.add_section('Upload')
_PARSER.set('Upload', 'Interval', '5')
_PARSER.set('Upload', 'Url', 'http://sensors.anyscale.org/push')
_PARSER.set('Upload', 'SecretKey', '')

_PARSER.add_section('Meta')
_PARSER.set('Meta', 'Location', 'unset')

def init(config_name):
    """ Reads the default global Ports file, and then the unit-specific file as per the name given. """
    paths = [
        os.path.join(_CONFIG_DIR, 'ports.conf'),
        os.path.join(_CONFIG_DIR, '%s.conf' % config_name)
    ]
    _PARSER.read(paths)

def get(section_name, property_name):
    """ Returns the property as a string. """
    return _PARSER.get(section_name, property_name)

def getfloat(section_name, property_name):
    """ Returns the property as a boolean. """
    return _PARSER.getfloat(section_name, property_name)

def getint(section_name, property_name):
    """ Returns the property as a boolean. """
    return _PARSER.getint(section_name, property_name)

def getboolean(section_name, property_name):
    """ Returns the property as a boolean. """
    return _PARSER.getboolean(section_name, property_name)
