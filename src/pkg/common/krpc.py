from google.protobuf.service import RpcChannel
from google.protobuf.service import RpcController
from google.protobuf.service import RpcException

from threading import Thread, current_thread
from Queue import Queue, Empty
from time import sleep
import socket
import logging

import krpc_common

# configure module level logging
def configure_logging():
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.WARN)

    ch = logging.StreamHandler()
    ch.setLevel(logging.WARN)

    # previous shell formatted format string:
    # '\033[7m%(levelname)s\033[0m\033[2m %(asctime)s %(name)s \033[0m%(message)s\033[0m'
    formatter = logging.Formatter('%(levelname)s %(asctime)s %(name)s %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)

configure_logging()

class KrpcInsecureChannel(RpcChannel):
    """ The channel is used to connect to a remote service and handles the wire format
    and socket connections on the client side. """
    logger = logging.getLogger(__name__).getChild('channel')
    host = None
    port = None
    timeout = 2

    def __init__(self, host, port):
        self.host = host
        self.port = port

    def _DoCallMethod(self, method_descriptor, rpc_controller, request, response_class):
        """ Connects to the remote server, requests a method call, and receives the
        return value.

        This implementation creates a new connection for every method call, and cannot
        currently deal with streaming data in either direction.

        The wire format is as follows for each method:
        1 byte: method index within service
        2 bytes: request length
        length bytes: request data

        The server is expected to respond after reading (3 + length) bytes.

        The response wire format is the same, except there is no method descriptor field.

        Once all expected bytes have been read / written, both sides can cleanly
        terminate the connection.
        """

        response = None
        s = None

        self.logger.debug("InsecureChannel: Calling method %s." % (method_descriptor.name))

        # All socket and serialization is wrapped in this big try block
        # to ensure the socket is properly closed.
        # TODO: This could be cleaner with smaller more specific try blocks.
        try:
            index = method_descriptor.index

            # Convert the request object to a byte string and check the serialized length
            r = request.SerializeToString()

            if (len(r) > 0xffff):
                raise Exception("Message too long, max is 65535 bytes, message was %d bytes." % length)

            # Open a connection to the remote server
            s = socket.create_connection((self.host, self.port), self.timeout)

            # Send the index as a single byte
            s.sendall(chr(index))

            # Send the length and data fields on the socket
            krpc_common.send_length_and_data(s, r)

            # Receive the length field of the response on the socket
            response_length = krpc_common.receive_length(s)
            if response_length < 0:
                raise Exception("Connection closed or timed out while receiving length")

            # Receive the response data on the socket
            response_string = krpc_common.receive_data(s, response_length)
            if len(response_string) < response_length:
                raise Exception("Connection closed or timed out while receiving data")

            # reconstruct response object from response_class
            response = response_class()
            response.ParseFromString(response_string)
        except Exception as e:
            rpc_controller.SetFailed("Exception in _DoCallMethod: %s" % e)
        finally:
            if s != None:
                s.close()
            return response

    def CallMethod(self, method_descriptor, rpc_controller, request, response_class, done):
        """ Attempts to call the method and returns its result either as a return value
        or by calling a callback. Returns None on error.
        """

        # The current implementation always synchronously calls the method and
        # waits for a result before either returning it or calling the call
        # back. TODO: To make asynchronous calls more efficient, we could use a
        # thread pool or dispatch a new thread for each call. However, most of
        # the time these calls will be made from dedicated threads anyway in our
        # applications.
        response = self._DoCallMethod(method_descriptor, rpc_controller, request, response_class)

        # Return None on failure or if a `done` callback was given.
        if rpc_controller.Failed():
            self.logger.warn("RPC Failed: %s" % rpc_controller.ErrorText())
            return None
        else:
            if done != None:
                done(response)
                return None
            else:
                return response

class KrpcController(RpcController):
    """ This class is mainly here for compatibility with the (deprecated) proto2
    service implementation. It allows clients some insight into the current state of
    a progressing or completed RPC call.
    """
    canceled = False
    cancellation_started = False
    completed = False
    failed = False
    on_cancel = None
    reason = ""

    def Reset(self):
        """ Resets the RpcController to its initial state.
        """
        self.canceled = False
        self.cancellation_started = False
        self.completed = False
        self.failed = False
        self.on_cancel = None
        self.reason = ""

    def Failed(self):
        """ Returns true if the call failed. """
        return self.failed

    def ErrorText(self):
        """ Returns a human-readable description of the error. """
        return self.reason

    def StartCancel(self):
        """ Allows the client to request cancellation of the request.

        Currently does nothing.
        """

        # Ensure we do not try to cancel the same thing twice.
        if self.cancellation.started:
            return
        else:
            self.cancellation_started = True

        # TODO: If request runs in a separate thread, kill that thread and
        # call SetCanceled when it has died.

    def SetCanceled(self):
        """ To be called when the cancellation is actually completed.

        Cancellation is currently not implemented.
        """
        if not self.canceled:
            self.canceled = True
            if self.on_cancel != None:
                self.on_cancel()
                self.on_cancel = None

    def SetFailed(self, reason):
        """ Sets a failure reason.

        Called in the channel when there has been an error.
        """
        self.failed = True
        self.reason = reason
        if (self.on_cancel != None):
            self.on_cancel()
            self.on_cancel = None

    def IsCanceled(self):
        return self.canceled

    def NotifyOnCancel(self, callback):
        """ Sets a callback to invoke on cancel. """
        if self.canceled:
            callback()
            return

        assert self.on_cancel == None, "Can only have one cancellation callback."
        self.on_cancel = callback

class KrpcServer():
    servicer = None
    work_queue = None
    running = False
    s = None
    host = None
    port = None

    # TODO: hard coded number of worker threads for our thread pool.
    num_workers = 16

    logger = logging.getLogger(__name__).getChild('server')

    def __init__(self, servicer):
        self.servicer = servicer
        self.work_queue = Queue()

    def start(self):
        """Binds the server to the defined host and port and starts listening for requests.
        """
        assert self.s == None, "server must not already be listening"
        assert self.host != None and self.port != None, "server must have a host and port set"

        self.running = True

        # start the required number of worker threads
        self.worker_threads = []
        for i in range(self.num_workers):
            t = Thread(target=self._worker_thread)
            t.start()
            self.worker_threads.append(t)

        # start the server thread that accepts connections and adds them to a work queue.
        self.server_thread = Thread(target=self._serve_thread)
        self.server_thread.start()

    def stop(self, _):
        """ Stops serving requests and shuts down the server, blocks until all threads
        have exited naturally.
        """

        self.running = False
        self.server_thread.join()

        # TODO could signal a more abrupt shutdown to the worker threads or
        # replace the queue remaining queue items with poison pills
        for t in self.worker_threads:
            t.join()

    def add_insecure_port(self, port):
        """Add a port to listen on, must be in the format host:port.
        The current implementation only uses the last port added before calling serve.
        """
        (h, p) = port.split(":")
        self.host = h
        self.port = int(p)

    def serve_forever(self):
        """ start the server and wait until it is stopped. """
        if not self.running:
            self.start()
        self.server_thread.join()

    def _serve_thread(self):
        """ This thread continuously accepts new connections and puts them into the work queue."""

        # Create a new socket
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # Allow the network stack to re-use the address if it has been left in TIME_WAIT state.
        self.s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        # Set a timeout of 5 seconds for all blocking socket operations. After
        # this time blocking operations will raise a socket.timeout exception.
        self.s.settimeout(5)

        # Keep retrying until the socket is successfully bound to a network address (TCP Port).
        while self.running:
            try:
                self.s.bind((self.host, self.port))
                break
            except Exception as e:
                self.logger.warning("Failed (retrying) to bind to port %d, error: %s" % (self.port, e))
                sleep(5)

        # Start listening on the socket with a backlog of 10 connections.
        self.s.listen(10)

        # Keep accepting requests and putting them in the work queue for as long as the server is running.
        while self.running:
            try:
                (conn, addr) = self.s.accept()
                self.work_queue.put(conn)
            except socket.timeout:
                # A timeout is normal and expected if no client connects within this time.
                pass
            except Exception as e:
                # Other exceptions must be logged but are ignored to try accepting again.
                self.logger.warning("Accept failed, error: %s" % (e))
                pass

        # Close the socket when the server is stopped.
        self.logger.debug("_serve_thread closing socket.")
        self.s.close()

    def _worker_thread(self):
        """ Each worker thread repeatedly takes connections off the queue,
        handles them, and then closes the connection.
        """
        # We use a blocking get() on the work queue with a timeout, to allow us to
        # shut down the threads within a reasonable time when running is false.
        while self.running:
            try:
                # TODO: hard-coded timeout of five seconds.
                conn = self.work_queue.get(True, 5)
            except Empty:
                # If the queue was empty, go back to the top and wait for another item.
                continue

            try:
                self._handle(conn)
            except Exception as e:
                # Other exceptions must be logged but are ignored to try accepting again.
                self.logger.warning("_handle failed, error: %s" % (e))

                import traceback
                traceback.print_exc()
                pass
            finally:
                self.logger.debug("_worker_thread closing a connection")
                conn.close()

    def _handle(self, conn):
        """ Handle a single connection requesting one method call. """
        self.logger.debug("Server accepted a connection")

        index  = krpc_common.receive_index(conn)
        length = krpc_common.receive_length(conn)
        if length < 0:
            self.logger.warn("Did not receive length.")
            return

        data   = krpc_common.receive_data(conn, length)
        if len(data) < length:
            self.logger.warn("Did not receive a complete message.")
            return

        # TODO error handling on -1 / small len(data)?
        self.logger.debug("Server received index %d length %d (actual: %d)" % (index, length, len(data)))

        # Find the right method to call.
        service_descriptor = self.servicer.GetDescriptor()
        for method_descriptor in service_descriptor.methods:
            if method_descriptor.index == index:
                self.logger.debug("Server selected method descriptor %s" % method_descriptor.full_name)

                # Parse the serialized request message
                request_class = self.servicer.GetRequestClass(method_descriptor)
                self.logger.debug("Server trying to create request of class %s" % request_class)
                request = request_class()
                request.ParseFromString(data)

                # Call the method and get its return value
                self.logger.debug("Server parsed request.")
                controller = create_controller() # TODO: WHY, OH WHY?!
                response = self.servicer.CallMethod(method_descriptor, controller, request, None)

                # Serialize and send the method's response message
                self.logger.debug("Server received response from servicer's method.")
                response_str  = response.SerializeToString()
                krpc_common.send_length_and_data(conn, response_str)

                # The end
                return

        # If no matching method descriptor for the requested index exists we do
        # nothing and just close the connection - that should be enough to
        # signal an error to the client.

def insecure_channel(host, port):
    return KrpcInsecureChannel(host, port)

def create_server(servicer):
    return KrpcServer(servicer)

def create_controller():
    # TODO: clients should not need to know about the controller.
    # Maybe wrapping the Service in a Servicer class in GRPC was not such a bad idea.
    return KrpcController()
