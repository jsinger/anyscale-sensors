def receive_index(conn):
    """receives a single byte index from the socket,
    returns -1 if connection was closed early.
    """
    data = conn.recv(1)
    if len(data) == 0:
        return -1
    else:
        return ord(data)

def receive_length(conn):
    """ Receives the two-byte length field from the socket and returns the
    number represented by these two bytes. Returns -1 if the connection was
    closed early.
    """

    try:
        high = conn.recv(1)
        low = conn.recv(1)
        return (ord(high) << 8) | ord(low)
    except:
        return -1

def receive_data(conn, length):
    """ Receives a byte string of at least the given length from the connected socket.
    """

    data = ""
    num_bytes_read = 0

    if length == 0:
        return ''

    while (True):
        buf = conn.recv(1024)
        num_bytes_read += len(buf)
        data += buf

        if (len(buf) == 0) or (num_bytes_read >= length):
            return data

def send_length_and_data(conn, data):
    """ Sends the two bytes length field, and then the message data itself,
    on the given connected socket.
    """

    length = len(data)

    # send the high byte of the length
    conn.sendall(chr((length >> 8) & 0xff))

    # send the low byte of the length
    conn.sendall(chr(length & 0xff))

    # send the message
    conn.sendall(data)
