from ..protocols import collector_pb2

def sensor_type_name(t):
    s = collector_pb2.SensorStatus

    if t == s.CUSTOM:
        return 'custom'
    elif t == s.TEMPERATURE:
        return 'temperature'
    elif t == s.LIGHT:
        return 'light'
    elif t == s.MOTION:
        return 'motion'
    elif t == s.SOUNDLEVEL:
        return 'soundlevel'
    else:
        return 'unknown'

