# Heartbeat Service

Implements the _heartbeat service_. Requires a _MongoDB_ connection for the _NodeCollection_.

Currently, this implementation directly updates the timestamp in the database entry for the given node. It also adds any reported active sensors not already in the node's list of sensor types to that list.

In the future this application should also be able to reconfigure a node if its reported configuration, such as intervals, does not match that stored in the database or if an immediate report has been requested.
