# I2C Service

Implements the _I2C service_.

Responds to requests when the bus transaction has been processed. Services requests in the order they arrived.
