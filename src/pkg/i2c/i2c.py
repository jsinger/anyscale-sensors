from ..protocols import i2c_pb2
from ..common import krpc as implementations
from ..common import config
from threading import BoundedSemaphore
import smbus

""" The I2c server serializes and executes grouped I2C transactions. """

class I2cService(i2c_pb2.I2cService):
    bus = None
    bus_lock = None

    def __init__(self, bus_number):
        """ Initialize the Bus and the Semaphore protecting it. """
        i2c_pb2.I2cService.__init__(self)

        self.bus = smbus.SMBus(bus_number)
        self.bus_lock = BoundedSemaphore()

    def Transaction(self, rpc_controller, request, context):
        """ Handler for the Transaction method of the I2CService interface. Executes
        the commands and replies with a list of responses. """
        results = []

        # acquire the lock and execute the entire sequence of commands before releasing it.
        with self.bus_lock:
            for cmd in request.commands:
                result = 0
                try:
                    if cmd.action == cmd.READ_BYTE:
                        result = self.bus.read_byte(cmd.addr)
                    elif cmd.action == cmd.WRITE_BYTE:
                        self.bus.write_byte(cmd.addr, cmd.data)
                    elif cmd.action == cmd.READ_BYTE_DATA:
                        result = self.bus.read_byte_data(cmd.addr, cmd.cmd)
                    elif cmd.action == cmd.WRITE_BYTE_DATA:
                        self.bus.write_byte_data(cmd.addr, cmd.cmd, cmd.data)
                    elif cmd.action == cmd.READ_WORD_DATA:
                        result = self.bus.read_word_data(cmd.addr, cmd.cmd)
                    elif cmd.action == cmd.WRITE_WORD_DATA:
                        self.bus.write_word_data(cmd.addr, cmd.cmd, cmd.data)
                    else:
                        print "Unsupported command %s." % cmd.action
                except Exception as e:
                    print "Error processing I2C command:%s\n%s" % (e, cmd)
                finally:
                    results.append(result)

        return i2c_pb2.I2cResponses(data_read = results)

def main():
    # Get configuration values
    config.init('i2c')
    port = (config.get('I2c', 'Host'), config.getint('I2c', 'Port'))
    i2c_bus_number = config.getint('I2c', 'BusNumber')

    # Start listening for TCP requests.
    print "I2C service listening on %s:%d." % port
    server = implementations.create_server(I2cService(i2c_bus_number))
    server.add_insecure_port('%s:%d' % port)
    server.serve_forever()

if (__name__ == "__main__"):
    main()
