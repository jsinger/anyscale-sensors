# Measurements Service

Implements the _measurements service_. Requires a _MongoDB_ connection for the `SensorReadingCollection`.

The current implementation directly inserts the received readings into the database. Reports are acknowledged immediately, before they are stored in the database.
