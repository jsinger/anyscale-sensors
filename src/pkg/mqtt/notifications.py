from ..common import config

import paho.mqtt.client as mqtt

def main():
    config.init('mqtt')

    mqtt_hostname = config.get('Mqtt', 'Hostname')
    mqtt_port = config.get('Mqtt', 'Port')
    mqtt_auth_token = config.get('Mqtt', 'AuthToken')
    mqtt_client_id = config.get('Mqtt', 'ClientId')


    def on_connect(client, userdata, flags, rc):
        client.subscribe("ot-2/type/python-gateway/id/aukena/notify")

    def on_message(client, userdata, msg):
        print msg.topic + " " + msg.payload

    client = mqtt.Client(client_id=mqtt_client_id)
    client.on_connect = on_connect
    client.on_message = on_message
    client.username_pw_set('use-token-auth', password=mqtt_auth_token)
    client.connect(mqtt_hostname, mqtt_port)

    client.loop_forever()

if __name__ == '__main__':
    main()
