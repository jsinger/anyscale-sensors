# REST API

## Implemented API Endpoints

The REST API currently only supports HTTP GET requests and returns data in a JSON format. For the moment, it is hosted at the base URL `http://aukena:5000/` and available from any host within the dcs domain.

### Node Listing

`/node/`

Returns a list of all registered sensor nodes in the database. A node's availability can be determined from the heartbeat and server timestamps. The names can be used to query more detailed information about a single node, see below.

```json{
"nodes": [{
		"heartbeat_timestamp": 1460720178,
		"sensor_types": ["temperature", "light", "motion"],
		"name": "pi5"
	}, {
		"heartbeat_timestamp": 1460720180,
		"sensor_types": ["temperature", "light", "motion"],
		"name": "pi4"
	}],
	"server_timestamp": 1460720183
}
```

### Node details

`/node/<node-name>/`

Returns detailed information about the sensor node including the most recent readings from each sensor attached to it.

```json
{
	"sensors": [{
		"sensor_type": "temperature",
		"readings": [{
			"timestamp": 1460716755,
			"sensor_type": "temperature",
			"value": 23.5625,
			"node_name": "pi4"
		}, {
			"timestamp": 1460716752,
			"sensor_type": "temperature",
			"value": 23.5625,
			"node_name": "pi4"
		}, {
			"timestamp": 1460716749,
			"sensor_type": "temperature",
			"value": 23.5625,
			"node_name": "pi4"
		}, {
			"timestamp": 1460716746,
			"sensor_type": "temperature",
			"value": 23.5625,
			"node_name": "pi4"
		}, {
			"timestamp": 1460716743,
			"sensor_type": "temperature",
			"value": 23.5625,
			"node_name": "pi4"
		}]
	}, {
		"sensor_type": "light",
		"readings": [ ... ]
	}, {
		"sensor_type": "motion",
		"readings": [ ... ]
	}],
	"heartbeat_timestamp": 1460720271,
	"name": "pi4"
}
```

### Sensor readings

`/node/<node-name>/<sensor-type>/`

`/node/<node-name>/<sensor-type>/?limit=<limit>`

`/node/<node-name>/<sensor-type>/?start=<timestamp>&end=<timestamp>`

Returns a list of sensor readings from an specific sensor, on a specific node. Optionally, a limit of the number of readings requested (default: 5, maximum: 100), and timestamp range (default: all starting with most recent), can be given. Reading objects include a timestamp and are generally returned in descending order, i.e. most recent first.

```json
{
	"sensor_type": "temperature",
	"readings": [{
		"timestamp": 1460716755,
		"sensor_type": "temperature",
		"value": 23.5625,
		"node_name": "pi4"
	}, {
		"timestamp": 1460716752,
		"sensor_type": "temperature",
		"value": 23.5625,
		"node_name": "pi4"
	}, {
		"timestamp": 1460716749,
		"sensor_type": "temperature",
		"value": 23.5625,
		"node_name": "pi4"
	}, {
		"timestamp": 1460716746,
		"sensor_type": "temperature",
		"value": 23.5625,
		"node_name": "pi4"
	}, {
		"timestamp": 1460716743,
		"sensor_type": "temperature",
		"value": 23.5625,
		"node_name": "pi4"
	}]
}
```
