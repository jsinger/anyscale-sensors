from flask import Flask, g
from flask.ext.restful import Api, Resource
from pymongo import MongoClient

from resources.node import NodeList, Node, NodeSensor

app = Flask(__name__)
api = Api(app)

# Default options
app.config.MONGO_HOST = 'localhost'
app.config.MONGO_PORT = 27017
app.config.MONGO_DATABASE = 'AnyscaleSensors'

# Open and close the database connection before and after each request
def connect_db():
    print "Connecting to database on %s:%d" % (app.config.MONGO_HOST, app.config.MONGO_PORT)
    client = MongoClient(app.config.MONGO_HOST, app.config.MONGO_PORT)
    database = client[app.config.MONGO_DATABASE]
    return database

@app.before_request
def before_request():
    g.db = connect_db()

@app.after_request
def add_allow_origin(response):
    response.headers['Access-Control-Allow-Origin'] = '*'
    return response

@app.teardown_request
def teardown_request(response):
    db = getattr(g, 'db', None)
    if db is not None:
        db.client.close()
    return response

# Endpoints for accessing a list of all nodes and
# information about current and historical status by node
api.add_resource(NodeList, '/node/')
api.add_resource(Node, '/node/<string:node_id>/')
api.add_resource(NodeSensor, '/node/<string:node_id>/<string:sensor_type>/')

if __name__ == '__main__':
    app.run(debug=True)
