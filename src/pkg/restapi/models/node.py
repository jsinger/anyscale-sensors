from flask import g
import pymongo
import time

def get_all_nodes():
    all_nodes = g.db.NodeCollection.find()
    nodes = []
    for node in all_nodes:
        print node
        nodes.append(node)

    return { 'nodes' : nodes }

def get_node(node_id):
    node = g.db.NodeCollection.find_one({'name': node_id})
    return node

def get_node_sensor_readings(node_id, sensor_type, start_timestamp=None, end_timestamp=None, limit=5):
    print node_id, sensor_type

    query = {
        'node_name': node_id,
        'sensor_type': sensor_type
    }

    if start_timestamp or end_timestamp:
        query['timestamp'] = {
            '$gte': start_timestamp or 0,
            '$lte': end_timestamp or int(time.time())
        }

    # Ensure that we have created an index to efficiently support descending sorted range queries.
    # The index ends with the timestamp field on which we sort, so that we can narrow the selection to
    # the node and sensor before any sorting.
    g.db.SensorReadingCollection.ensure_index([('node_name', 1), ('sensor_type', 1), ('timestamp', -1)])

    readings = g.db.SensorReadingCollection.find(query, limit = limit).sort('timestamp', pymongo.DESCENDING)

    # explain = readings.explain()
    # print "millis: %dms, nscanned:\t %d, cursor: %s" % (
    #     explain['millis'],
    #     explain['nscanned'],
    #     explain['cursor']
    # )

    readings_list = []
    for reading in readings:
        readings_list.append(reading)
    return readings_list
