from flask.ext.restful import Resource, reqparse, fields, marshal_with, abort
from ..models.node import get_node, get_all_nodes, get_node_sensor_readings
import time

parser = reqparse.RequestParser()
parser.add_argument('start', type=int, help='Earliest timestamp to return')
parser.add_argument('end', type=int, help='Latest timestamp to return')
parser.add_argument('limit', type=int, help='Maximum number of readings to return')

fields_node_meta = {
    'location': fields.String
}

fields_short_node = {
    'name': fields.String,
    'heartbeat_timestamp': fields.Integer,
    'sensor_types': fields.List(fields.String),
    'meta': fields.Nested(fields_node_meta)
}

fields_nodes_list = {
    'server_timestamp': fields.Integer,
    'nodes': fields.List(fields.Nested(fields_short_node))
}

fields_sensor_reading = {
    'node_name': fields.String,
    'sensor_type': fields.String,
    'timestamp': fields.Integer,
    'value': fields.Float
}

fields_sensor = {
    'sensor_type': fields.String,
    'readings': fields.List(fields.Nested(fields_sensor_reading))
}

fields_long_node = {
    'name': fields.String,
    'heartbeat_timestamp': fields.Integer,
    'heartbeat_timestamp': fields.Integer,
    'sensors': fields.List(fields.Nested(fields_sensor)),
    'meta': fields.Nested(fields_node_meta)
}

class NodeList(Resource):
    @marshal_with(fields_nodes_list)
    def get(self):
        '''
        Returns a list of all registered nodes in the system.
        '''
        nodes = get_all_nodes()
        nodes['server_timestamp'] = int(time.time())
        return nodes

class Node(Resource):
    @marshal_with(fields_long_node)
    def get(self, node_id):
        '''
        Returns the current configuration of the chosen node.
        '''
        node = get_node(node_id)
        if node == None:
            abort(404, message="No such node.")

        if not node.has_key('sensors'):
            node['sensors'] = []

        for sensor_type in node['sensor_types']:
            print "Sensor type is %s." % sensor_type
            node['sensors'].append({
                'sensor_type': sensor_type,
                'readings': get_node_sensor_readings(node_id, sensor_type)
            })

        return node

class NodeSensor(Resource):
    @marshal_with(fields_sensor)
    def get(self, node_id, sensor_type):
        '''
        Returns readings of this sensor_type for this node_id.
        The query string may optionally specify a from/to timestamp range.
        '''
        args  = parser.parse_args()
        start = args.start
        end   = args.end
        limit = args.limit

        if limit == None:
            limit = 5
        elif limit > 1000:
            limit = 1000

        node = get_node(node_id)

        if (node == None) or (sensor_type not in node['sensor_types']):
            abort(404, message="No such sensor.")

        readings = get_node_sensor_readings(node_id,
                                            sensor_type,
                                            limit=limit,
                                            start_timestamp=start,
                                            end_timestamp=end)
        return {
            'node_name': node_id,
            'sensor_type': sensor_type,
            'readings': readings
        }
