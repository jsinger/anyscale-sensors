import argparse
from gevent.wsgi import WSGIServer
from app import app
from ..common import config

def main():
    config.init('restapi')

    port = (config.get('Restapi', 'Host'), config.getint('Restapi', 'Port'))

    app.config.MONGO_HOST = config.get('Mongo', 'Host')
    app.config.MONGO_PORT = config.getint('Mongo', 'Port')
    app.config.MONGO_DATABASE = config.get('Mongo', 'Database')

    if (config.getboolean('Restapi', 'Debug')):
        app.debug = True

    http_server = WSGIServer(port, app)
    print "REST API Server listening on %s:%d." % port
    http_server.serve_forever()

if __name__ == '__main__':
    main()
