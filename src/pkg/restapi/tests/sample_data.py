import time
import random

def generate_sensor_readings(num, spec):
    readings = []
    for i in range(num):
        (node_name, sensor_type, unit) = random.choice(spec)
        readings.append({
            'node_name' : node_name,
            'sensor_type' : sensor_type,
            'unit': unit,
            'timestamp': int(time.time()) - random.randint(0, 600),
            'value': random.random() * 100
        })
    return readings

def generate(app):
    db = app.connect_db()

    db.NodeCollection.insert_one({
        'name': 'pi1',
        'public_key': '1234',
        'enabled': True,
        'heartbeat_period': 100,
        'reporting_period': 300,
        'sensors': [
            {
                'sensor_type': 'temperature',
                'polling_period': 60
            },
            {
                'sensor_type': 'light',
                'polling_period': 20
            }
        ]
    })

    db.NodeCollection.insert_one({
        'name': 'pi2',
        'public_key': '5678',
        'enabled': True,
        'heartbeat_period': 100,
        'reporting_period': 300,
        'sensors': [
            {
                'sensor_type': 'temperature',
                'polling_period': 60
            },
            {
                'sensor_type': 'light',
                'polling_period': 20
            },
            {
                'sensor_type': 'motion'
            },
        ]
    })


    db.SensorReadingCollection.insert_many(generate_sensor_readings(100, [
        ('pi1', 'temperature', 'celsius'),
        ('pi1', 'light', 'lux'),
        ('pi2', 'motion', 'active'),
        ('pi2', 'temperature', 'celsius'),
        ('pi2', 'motion', 'active')
    ]))


    db.client.close()
