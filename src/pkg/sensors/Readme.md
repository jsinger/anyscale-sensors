# Sensors

The sensors all connect to the _collector service_, and some of them also make use of the _I2C service_.

Current implementations inherit timing and reporting functionality from `PollingSensor` and only have to implement their own specific `sense` method.
