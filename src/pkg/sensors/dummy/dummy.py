from ..polling_sensor import PollingSensor
from ...common import config

import random

class DummySensor(PollingSensor):
    value_type = float
    sensor_type = PollingSensor.SENSOR_TYPES.CUSTOM

    def get_sensor_value(self):
        return random.randint(0, 100) / 10.0

def main():
    sensor = DummySensor()
    sensor.run()

if (__name__ == "__main__"):
    main()
