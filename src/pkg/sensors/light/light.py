from ...protocols import i2c_pb2
from ...common import krpc as implementations
from ...common import config
from ..polling_sensor import PollingSensor

_DEVICE_ADDR = 0x39

class LightSensor(PollingSensor):
    value_type = float
    sensor_type = PollingSensor.SENSOR_TYPES.LIGHT
    i2c = None

    def get_sensor_value(self):
        transaction = i2c_pb2.I2cTransaction()

        # flags to select command register, select word mode.
        # low 4 bits must be filled in with register address below.
        cmd = 0b10100000

        # Read Channel 0
        command = transaction.commands.add()
        command.action = command.READ_WORD_DATA
        command.addr   = _DEVICE_ADDR
        command.cmd    = cmd | 0xC

        # Read Channel 1
        command = transaction.commands.add()
        command.action = command.READ_WORD_DATA
        command.addr   = _DEVICE_ADDR
        command.cmd    = cmd | 0xE

        controller = implementations.create_controller()
        results = self.i2c.Transaction(controller, transaction, None)

        if results != None and len(results.data_read) == len(transaction.commands):
            ch0 = results.data_read[0]
            ch1 = results.data_read[1]
            return self._data_to_lux(ch0, ch1)
        else:
            print "Problem with response from i2c service: %s" % results
            return None

    def _power_up(self):
        transaction = i2c_pb2.I2cTransaction()

        command = transaction.commands.add()
        command.action = command.WRITE_BYTE_DATA
        command.addr   = _DEVICE_ADDR
        command.cmd    = 0x00
        command.data   = 0x03

        controller = implementations.create_controller()
        self.i2c.Transaction(controller, transaction, None)
        print "power up requested"

    def _data_to_lux(self, ch0, ch1):
        # Using data for T/FN/CL package
        lux = 0

        if (ch0 == 0):
            return lux

        ratio =  1.0 * ch1/ch0

        if ratio < 0.50:
            lux = 0.0304 * ch0 - 0.062 * ch0 * (ratio ** 1.4)
        elif ratio < 0.61:
            lux = 0.0224 * ch0 - 0.031 * ch1
        elif ratio < 0.80:
            lux = 0.0128 * ch0 - 0.0153 * ch1
        elif ratio < 1.30:
            lux = 0.00146 * ch0 - 0.00112 * ch1
        else: #ratio > 1.30
            Lux = 0
        return lux

    def additional_setup(self):
        i2c_host = config.get('LightSensor', 'I2cHost')
        i2c_port = config.get('I2c', 'Port')

        channel = implementations.insecure_channel(i2c_host, i2c_port)
        self.i2c = i2c_pb2.I2cService_Stub(channel)
        self._power_up()

def main():
    config.init('sensor_light')

    sensor = LightSensor()
    sensor.run()

if (__name__ == "__main__"):
    main()
