from ..polling_sensor import PollingSensor
from ...common import config

import RPi.GPIO as GPIO

class MotionSensor(PollingSensor):
    value_type = bool
    sensor_type = PollingSensor.SENSOR_TYPES.MOTION
    motion_triggered = False
    motion_triggered_since_last_reading = False
    sensor_pin = 22

    def handleEdge(self, pin):
        if (GPIO.input(self.sensor_pin)):
            print 'UP'
            self.motion_triggered = True
            self.motion_triggered_since_last_reading = True
        else:
            print 'DOWN'
            self.motion_triggered = False

    def get_sensor_value(self):
        value = self.motion_triggered_since_last_reading
        self.motion_triggered_since_last_reading = self.motion_triggered
        return value

    def additional_setup(self):
        self.sensor_pin = config.getint('MotionSensor', 'SensorPin')

        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.sensor_pin, GPIO.IN)

        GPIO.add_event_detect(self.sensor_pin, GPIO.BOTH)
        GPIO.add_event_callback(self.sensor_pin, self.handleEdge)

def main():
    config.init('sensor_motion')

    sensor = MotionSensor()

    try:
        sensor.run()
    finally:
        GPIO.cleanup()

if (__name__ == "__main__"):
    main()
