from ..protocols import collector_pb2
from ..common import krpc as implementations
from ..common import config
import time
import threading

class PollingSensor():
    SENSOR_TYPES = collector_pb2.SensorStatus
    sensor_type = SENSOR_TYPES.CUSTOM
    value_type = int
    sensor_value = 0
    report_period = 3
    last_report_time = 0
    report_timer = None
    report_unchanged = True
    report_unchanged_once = True
    last_reported_value = None
    error = False

    collector_channel = None
    collector = None

    def __init__(self):
        self.report_event = threading.Event()
        self.heartbeat_event = threading.Event()

    def report_thread(self):
        """ Send a report when the report_event is triggered and then reset the
        report timer to trigger it again. """

        while(True):
            # Wait until the report event is set
            self.report_event.wait()
            self.report_event.clear()

            # Query the sensor for the current value
            self.sense()

            # Prepare and send a report
            self.report()

            # Set a timer for the next report.
            self.reset_report_timer()

    def sense(self):
        """ Read the sensor value from the subclass's get_sensor_value implementation
        and handle potential errors by logging them and setting an error flag. """

        # Clear the error flag
        self.error = False

        # Get sensor value and set error flag if there is an exception not handled
        # by the subclass
        try:
            self.sensor_value = self.get_sensor_value()
        except Exception as e:
            print e
            self.error = True

        # Also set the error flag if the reported value is None
        if self.sensor_value == None:
            self.error = True

    def report(self):
        """ Determine whether to send a report based on the current sensor value, and send it if needed.
        """

        # If there was an error, do not send a report.
        if (self.error == True) or (self.sensor_value == None):
            return

        # If the value is unchanged and report_unchanged is not set, do not send a report.
        if (self.sensor_value == self.last_reported_value) and (not self.report_unchanged) and (not self.report_unchanged_once):
            return

        # Initialize a sensor status message
        status = collector_pb2.SensorStatus()
        status.timestamp = int(time.time())
        status.sensor_type = self.sensor_type

        # Assign the current sensor value to the correct field in the message
        if self.value_type == int:
            status.int_value = self.sensor_value
        elif self.value_type == bool:
            status.bool_value = self.sensor_value
        elif self.value_type == float:
            status.float_value = self.sensor_value
        else:
            # Unsupported value type, do not send a report with an empty value.
            print "Unsupported sensor value type, %s (value: %s)" % (self.value_type, self.sensor_value)
            return

        # Store the current value for later determining if it has changed.
        self.last_reported_value = self.sensor_value

        # Do the RPC call to send a report.
        controller = implementations.create_controller()
        response = self.collector.Report(controller, status, None)

        # when there was an error connecting to the collector we should try again with next reading
        # even if the value has not changed.
        if (controller.Failed()):
            self.report_unchanged_once = True
        else:
            self.report_unchanged_once = False

    def heartbeat_thread(self):
        """ Send a heartbeat when the heartbeat_event is triggered and then reset
        the timer to trigger it again. """

        while(True):
            # Wait until the report event is set
            self.heartbeat_event.wait()
            self.heartbeat_event.clear()

            # Prepare and send a heartbeat, and handle the config changes from the response.
            self.heartbeat()

            # Set a timer for the next heartbeat.
            self.reset_heartbeat_timer()

    def heartbeat(self):
        """ Send a heartbeat and reconfigure the sensor if requested in the response to it. """
        controller = implementations.create_controller()
        heartbeat = collector_pb2.HeartbeatMessage(sensor_type=self.sensor_type)
        reply = self.collector.Heartbeat(controller, heartbeat, None)

        # TODO handle reconfiguration request in reply.

    def reset_report_timer(self):
        """ Restart the report timer to trigger the next report after the current period. """
        # Cancel existing timer
        if self.report_timer != None:
            self.report_timer.cancel()

        # Start a new timer
        self.report_timer = threading.Timer(self.report_period, self.trigger_report)
        self.report_timer.start()

    def reset_heartbeat_timer(self):
        """ Restart the heartbeat timer to trigger the next heartbeat after the current period. """
        # Cancel existing timer
        if self.heartbeat_timer != None:
            self.heartbeat_timer.cancel()

        # Start a new timer
        self.heartbeat_timer = threading.Timer(self.heartbeat_period, self.heartbeat_report)
        self.heartbeat_timer.start()

    def trigger_report(self):
        """ Triggers the report event to allow the report_thread to continue. """
        self.report_event.set()

    def trigger_heartbeat(self):
        """ Triggers the heartbeat event to allow the heartbeat_thread to continue. """
        self.report_event.set()

    def additional_setup(self):
        """ Subclasses should override this to perform additional initializations in run(). """
        pass

    def connect_collector(self):
        """ Initialize the channel for connecting to the collector service. """
        port = config.getint('Collector', 'Port')
        print "Connecting to collector service on %d." % port
        self.channel = implementations.insecure_channel('localhost', port)
        self.collector = collector_pb2.CollectorService_Stub(self.channel)

        controller = implementations.create_controller()

    def run(self):
        """ Starts the sensor threads and blocks forever. """

        # Get configuration parameters
        self.report_period    = config.getfloat('Sensor', 'ReportPeriod')
        self.heartbeat_period = config.getfloat('Sensor', 'HeartbeatPeriod')
        self.report_unchanged = config.getboolean('Sensor', 'ReportUnchanged')

        # Initialize the collector connection
        self.connect_collector()

        # perform additional, sensor-specific setup steps
        self.additional_setup()

        # Immediately trigger the first report and heartbeat
        self.trigger_report()
        self.trigger_heartbeat()

        # Initialize and start the heartbeat and reporting threads
        t_heartbeat = threading.Thread(target=self.heartbeat_thread)
        t_report    = threading.Thread(target=self.report_thread)
        t_heartbeat.start()
        t_report.start()

        # Join these threads to block the main thread forever
        # TODO: Ideally we should monitor them and exit the main program on a failure.
        t_heartbeat.join()
        t_report.join()
