from ..polling_sensor import PollingSensor
from ...common import config

import time
import spidev

_SPI_DEVICE = 0
_SPI_CHANNEL = 0
_SPI_SPEED_HZ = 3000000

_SOUND_SENSOR_SAMPLING_HZ_ = 20        #How many (times/second) should we sample
_SOUND_SENSOR_SAMPLING_PERIOD_ = 1     #Total time to sample sound

class SoundlevelSensor(PollingSensor):
    value_type = float
    sensor_type = PollingSensor.SENSOR_TYPES.SOUNDLEVEL

    def __init__(self, freq = _SOUND_SENSOR_SAMPLING_HZ_ , period = _SOUND_SENSOR_SAMPLING_PERIOD_):
        # Invoke super class constructor
        PollingSensor.__init__(self)

        self.sound_samples = []
        self.sample_freq = freq
        self.sample_period = period

    def get_sensor_value(self):
        """ Function returns median of samples observed over a time period,
            blocking until all samples have been collected.
            Return Value : median of all samples within specified period
            Notes        : Function internally
                           i)   opens spi bus, obtains
                           ii)  samples at frequency of 'sample_freq'
                                over a period of 'sample_period' and
                           iii) closes spi-bus
        """
        try:
            sleep_time = (1.0/self.sample_freq)
            no_of_samples = (self.sample_period / sleep_time)
            samples = []

            # Open SPI bus
            self._open_spi_bus()

            # Collect samples at a regular interval
            for idx in range(0, int(no_of_samples) ):
                samples.append( self._get_instantaneous_value() )
                time.sleep( sleep_time )

            # TODO: debugging log, remove
            print samples

            # return the median value of the captured samples
            return self._median(samples)
        except Exception as e:
            # Return None on error
            print "Error in get_sensor_value: %s" % e
            return None
        finally:
            # Always close the SPI bus
            self._close_spi_bus()

    def _get_instantaneous_value(self):
        """ Internal function to request a single immediate sample from the opened SPI device.
            Return Value: integer value converted from bytes read from ADC
        """
        try:
            bytes = self.spi.xfer(self.to_send)
            return self._adc_value(bytes)
        except Exception:
            return None

    def _open_spi_bus(self):
        """ Internal class function to set-up SPI channel parameters
            and to open handle to SPI bus
            Return Value : A handle to requisite spi bus
        """
        self.spi = spidev.SpiDev()
        self.to_send = [0xd0, 0x00, 0x00]

        try:
            self.spi.open(_SPI_DEVICE, _SPI_CHANNEL)
        except Exception:
            return None
        finally:
            return None

    def _close_spi_bus(self):
        """ Internal class function to close SPI channel previously opened
            Return Value : None
        """
        try:
            self.spi.close()
        except Exception:
            return None
        finally:
            return None

    def _adc_value(self, bytes):
        """ Internal class method, converts the list of 2 bytes read from the ADC to an integer. """
        high = bytes[0] & 0b0111
        low = bytes[1] >> 1
        return high << 7 | low

    def _median(self, samples):
        """ returns the median value of the given list. """
        v = samples[:]
        length = len(v)
        v.sort()

        if length < 1:
            return None
        elif length % 2 == 0:
            # length is even, return average of two middle items
            return (v[length / 2] + v[length / 2 - 1] ) / 2.0
        else:
            # length is odd, return the middle or only item
            return v[(length - 1) / 2]

def main():
    config.init('sensor_soundlevel')

    period = config.getfloat('SoundlevelSensor', 'SamplingPeriod')
    freq = config.getfloat('SoundlevelSensor', 'SamplingFrequency')

    sensor = SoundlevelSensor(freq=freq, period=period)
    sensor.run()

if (__name__ == "__main__"):
    main()
