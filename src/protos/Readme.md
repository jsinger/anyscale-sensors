# Services and Protocols

Services and message types are defined using the _proto3_ syntax for Google's [Protocol Buffers](https://developers.google.com/protocol-buffers/) wire format. 

## List of Services

### Collector

The **collector service** runs on a sensor node and aggregates readings from all connected sensors before sending reports at a fixed interval. Sensors communicate with this service by sending _heartbeats_ and _reports_. The collector can reply with sensor reconfiguration messages (not implemented).

### I2C

The **I2C service** runs on a sensor node and serializes transactions for the I2C bus that is shared between different sensor implementations. Sensors communicate with this service by sending a _Transaction_ consisting of a list of requests. The I2C service responds, after running the transaction, with a list of data words read in response to these bus requests.

### Heartbeat

The **heartbeat service** runs on the main server and collects status information from the sensor nodes. Collector implementations send regular _Heartbeat_ messages, and receive _Heartbeat Replies_ which may contain reconfiguration options.

### Measurements

The **measurements service** runs on the main server and collects sensor status reports from the sensor nodes. Collector implementations send regular _Reports_ made up of _Sensor Status_ items, and receive back a simple _acknowledgement_.

## Implementation

### Manual implementation
Python stubs (clients) and base classes (servers) can be generated from these descriptions using the _protoc_ compiler. These will be imported and implemented in the various server and client applications.

### GRPC Implementation (not currently used)

The [gRPC](http://www.grpc.io/docs/) python library currently does not work on the Raspberry Pi. However, we hope to migrate to it as soon as that platform is supported. The gRPC plugin can be used to generate server and client stubs for use with this implementations as follows:

```
GRPC_ROOT=/Users/kristian/stuff/protobuf/grpc-release-0_13_0/
protoc --python_out=. --grpc_out=. --plugin=protoc-gen-grpc=$GRPC_ROOT/bins/opt/grpc_python_plugin -I protos/ protos/*.proto 
```

#### Prerequisites

The development machine needs an installed version of the _protoc_ compiler on the path, and the _grpc_python_plugin_ compiled from the _grpc_ repository.

1. [protobuf v3.0.0-beta-2](https://github.com/google/protobuf/archive/v3.0.0-beta-2.tar.gz): Build and install the compiler and C++ runtime, `./autogen.sh && ./configure && make && sudo make install`
2. [grpc release-0.13.0](): `make grpc_python_plugin`

I also installed the grpcio and protobuf runtimes to be able to run the generated code.

* Install the grpcio runtime from PyPI: `pip install grpcio`
* [Install the protobuf python runtime](https://github.com/google/protobuf/tree/master/python).
