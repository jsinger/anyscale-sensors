#!/bin/sh

I2C_PORT=50010
COLLECTOR_PORT=50020
HEARTBEAT_PORT=50030
MEASUREMENTS_PORT=50040

SERVER_HOST=pi6

# set up ssh tunnel to SERVER_HOST
ssh -N -f -L $MEASUREMENTS_PORT:localhost:$MEASUREMENTS_PORT $SERVER_HOST
ssh -N -f -L $HEARTBEAT_PORT:localhost:$HEARTBEAT_PORT $SERVER_HOST

source venv/bin/activate

sleep 5

echo "starting services"

# Run sensor services
(
    python -m pkg.collector.collector &
    python -m pkg.i2c.i2c &
    python -m pkg.measurements.measurements &
    python -m pkg.sensors.dummy.dummy --collector-port=$COLLECTOR_PORT &
    python -m pkg.sensors.light.light --i2c-port=$I2C_PORT --collector-port=$COLLECTOR_PORT &
    python -m pkg.sensors.temperature.temperature --i2c-port=$I2C_PORT --collector-port=$COLLECTOR_PORT &
    python -m pkg.sensors.motion.motion --collector-port=$COLLECTOR_PORT &
) > /dev/null 2>&1 < /dev/null
