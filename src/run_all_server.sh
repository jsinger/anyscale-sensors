#!/bin/sh

SERVER_HOST=pi6

# Run server services
(
    ./run_measurements.sh &
    ./run_heartbeat.sh &
    ./run_restapi.sh &
) > /dev/null 2>&1 < /dev/null
