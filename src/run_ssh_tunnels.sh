#!/bin/bash

# TODO: This should be a python script that makes sure these tunnels are kept alive,
# and that gets the ports, user, and hostname from a config file.
ssh -N -f -L 50030:localhost:50030 -L 50040:localhost:50040 ansycalesensors_tunnel@aukena
