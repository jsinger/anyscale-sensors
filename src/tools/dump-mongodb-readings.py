import pymongo

def main():
    client = pymongo.MongoClient()
    db = client['AnyscaleSensors']

    cursor = db.SensorReadingCollection.find().sort([('node_name', pymongo.DESCENDING), ('sensor_type', pymongo.DESCENDING), ('timestamp', pymongo.ASCENDING)])
    for r in cursor:
        print "%s, %s, %s, %s" % (
            r['timestamp'],
            r['sensor_type'],
            r['node_name'],
            r['value']
        )

if __name__ == "__main__":
    main()
